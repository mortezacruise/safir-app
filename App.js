import React, {useEffect} from 'react';
import {View, Text, I18nManager} from 'react-native';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createMaterialTopTabNavigator} from 'react-navigation-tabs';
import {Provider as DataContext} from './src/context/DataContext';
import {Provider as AuthContext} from './src/context/AuthContext';
import {Provider as UserDataProvider} from './src/context/userDataContext';
import {setNavigator} from './navigationRef';
import AuthScreen from './src/screens/AuthScreen/AuthScreen';
import {Root} from 'native-base';
import SplashScreen from './src/screens/SplashScreen';
import {MAIN_COLOR} from './src/values/Colors';
import Icon from 'react-native-vector-icons/FontAwesome5';
import ClubsScreen from './src/screens/ClubsScreen';
import Comments from './src/screens/ClubsScreen/AboutContent/Comments';
import CategoriesScreen from './src/screens/CategoryScreen';
import BookmarkScreen from './src/screens/ProfileScreen/Bookmarks';
import EditProfile from './src/screens/ProfileScreen/EditProfile';
import ContactUs from './src/screens/ProfileScreen/ContactUs';
import ReportUser from './src/components/ReportUser';
import PushPole from 'pushpole-react-native';
import TermOfUse from './src/baseComponent/TermOfUse';
import CommonQuestion from './src/screens/ProfileScreen/CommonQuestion';
import {
  Provider as PaperProvider,
  DefaultTheme,
  DarkTheme,
} from 'react-native-paper';
import AllCategories from './src/screens/CategoryScreen/AllCategories';
const rtl = I18nManager.isRTL;
I18nManager.allowRTL(false);
PushPole.initialize(true);

const HomeStack = createStackNavigator(
  {
    Home: {getScreen: () => require('./src/screens/HomeScreen').default},
  },
  {defaultNavigationOptions: {headerShown: false}},
);

const ProFileStack = createStackNavigator(
  {
    ProfileScreen: {
      getScreen: () => require('./src/screens/ProfileScreen').default,
    },
  },
  {defaultNavigationOptions: {headerShown: false}},
);
const Tabs = createMaterialTopTabNavigator(
  {
    ProfileScreen: {
      screen: ProFileStack,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <Icon name="user" color={tintColor} size={25} />
        ),
      },
    },
    LocationScreen: {
      getScreen: () => require('./src/screens/LocationScreen').default,

      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <Icon name="street-view" color={tintColor} size={25} />
        ),
      },
    },
    Bookmark: {
      screen: BookmarkScreen,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <Icon name="bookmark" color={tintColor} size={25} />
        ),
      },
    },
    ListScreen: {
      screen: AllCategories,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <Icon name="list" color={tintColor} size={25} />
        ),
      },
    },

    HomeScreen: {
      screen: HomeStack,

      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <Icon name="home" color={tintColor} size={25} />
        ),
      },
    },
  },

  {
    swipeEnabled: false,
    lazy: true,
    tabBarPosition: 'bottom',
    initialRouteName: 'HomeScreen',
    backBehavior: 'initialRoute',
    tabBarOptions: {
      scrollEnabled: false,
      contentContainerStyle: {alignItems: 'center', justifyContent: 'center'},
      style: {backgroundColor: 'white'},
      showIcon: true,
      showLabel: false,
      activeBackgroundColor: '#fff',
      inactiveBackgroundColor: '#fff',
      activeTintColor: MAIN_COLOR,
      inactiveTintColor: '#aaa',
      iconStyle: {alignSelf: 'center', width: 30},
      pressColor: MAIN_COLOR,
      allowFontScaling: true,
      indicatorStyle: {backgroundColor: MAIN_COLOR},
    },
  },
);
const MainFlow = createStackNavigator(
  {
    Tabs,
    ClubsScreen,
    CommentScreen: {screen: Comments},
    Map: {getScreen: () => require('./src/screens/LocationScreen').default},
    CategoriesScreen,
    EditProfile,
    ReportUser,
    UserInfo: {
      getScreen: () =>
        require('./src/screens/ProfileScreen/UserInformation').default,
    },
    Bookmarks: {
      getScreen: () => require('./src/screens/ProfileScreen/Bookmarks').default,
    },
    TransAction: {
      getScreen: () =>
        require('./src/screens/ProfileScreen/TransactonScreen').default,
    },
    AboutUs: {
      getScreen: () => require('./src/screens/ProfileScreen/AboutUs').default,
    },
    ContactUs,
    TermOfUse,
    CommonQuestion,
    Search: {
      getScreen: () => require('./src/containers/SearchContainer').default,
    },
    More: {
      getScreen: () => require('./src/screens/MoreStoresInfifnity').default,
    },
  },

  {defaultNavigationOptions: {headerShown: false}, initialRouteName: 'Tabs'},
);
const AuthFlow = createStackNavigator(
  {
    AuthScreen,
    TermOfUse,
  },
  {defaultNavigationOptions: {headerShown: false}},
);
const switchNavigator = createSwitchNavigator({
  Splash: SplashScreen,
  AuthFlow: AuthFlow,
  // LoginFlow: AuthScreen,
  MainFlow,
});
const App = createAppContainer(switchNavigator);
export default () => {
  return (
    <AuthContext>
      <DataContext>
        <Root>
          <UserDataProvider>
            <PaperProvider>
              <App ref={(navigator) => setNavigator(navigator)} />
            </PaperProvider>
          </UserDataProvider>
        </Root>
      </DataContext>
    </AuthContext>
  );
};
