export const BASE_URL = 'https://safirccard.ir/api/v1';
export const HOME_ROUTE = BASE_URL + '/home';
export const CHECK_TOKEN = BASE_URL + '/checkToken';
export const GET_DEACTIVE_KAMPON = BASE_URL + '/owner/getDeactives';
export const GET_ACTIVE_KAMPON = BASE_URL + '/owner/getActives';
export const GET_SOLD_KAMPON = BASE_URL + '/owner/getUsed';
export const GET_UNSOLD_KAMPON = BASE_URL + '/owner/getNotUsed';
export const POST_SUBMIT_KAMPON = BASE_URL + '/owner/isUsed';
export const GET_TRANSACTION = BASE_URL + '/owner/transactions';
export const GET_OWNER_INFO = BASE_URL + '/owner/info';
export const WITHDRAW_REQUEST = BASE_URL + '/owner/withdrawalRequest';
export const GET_DISCOUNT_INFO = BASE_URL + '/d';
export const ADD_SHOW_CART = BASE_URL + '/cart';
export const ADD_BOOKMARK = BASE_URL + '/bookmark';
export const CATEGORY_INFINITIES = BASE_URL + '/d/sport/1';
export const CATEGORY_INFINITIES_SLIDER = BASE_URL + '/s';
export const REPORT = BASE_URL + '/member/report';
export const SUBCATS = BASE_URL + '/c/';

export const USE = 'USE';

export const ACTIVE = 'ACTIVE';
export const TOMAN = 'تومان';

export const INTRO_FIRST_TITLE = 'بیشترین تخفیف';
export const INTRO_SECOND_TITLE = 'صفحه ویژه';
export const INTRO_THIRTH_TITLE = 'خرید آسان';
export const INTRO_FORTH_TITLE = 'کمپُن';

export const INTRO_FIRST_DESCRIPTION =
  'همیشه میتونی بیشترین تخفیف هارو اینجا پیدا کنی';
export const INTRO_SECEND_DESCRIPTION =
  'میتونی تخفیفارو بر اساس پربازدیدترین ،پرتخفیف ترین ،و...فیلتر کنی';
export const INTRO_THIRTH_DESCRIPTION =
  'میتونی خیلی راحت با چند تا کلیک کمپُنِتُ بخری';
export const INTRO_FORTH_DESCRIPTION = 'با کمپُن خرجتُ کم کُن';
export const MOBILE_WRONG = 'لطفا موبایل خود را کامل وارد کنید';
//=================== Async storage constants ==========================
export const MOBILE_NUMBER = 'mobileNumber';
export const IS_LOGIN = 'isLogin';
export const SHOW_INTRO = 'showIntro';

export const FOR_YOU = 'جدید ترین ها';

//============== Response Codes ==================

export const AUTH_SUCCESS = 2022;
export const BOOKMARK_ADDED = 2023;
export const SUCCESS_EXIST_IN_CART = 2057;
export const SUCCESS_KAMPON_SUBMITED = 2098;
export const SUCCESS_GET_TRANSACTION = 2058;

//============== Check Codes ===========================
export const CART_EXIST = 5047;

//============== Delete Codes ===========================
export const DELETE_SUCESS = 3064;

export const SUBMIT_KAMPON = 'ثبت کمپن';
export const CANCEL = 'انصراف';
export const FEATUR = 'ویژگی ها';
export const TERM_OF_USE = 'شرایط استفاده';
export const TOKEN_WRONG = 'کد وارد شده صحیح نیست';
export const PLEASE_ENTER_CODE = 'کد دریافتی را کامل وارد کنید';
export const PLEASE_ENTER_MOBILE_NUMBER = 'شماره همراه خود را کامل وارد کنید';
export const MOBILE_SHOULD_START_WITH_09 = 'شماره همراه اشتباه است';
export const MOBILE_NOT_SUBMIT_IN_DB =
  'شماره شما در سامانه کمپُن موجود نیست با واحد پشتیبانی تماس بگیرید';
export const ENTER_TOKEN = 'کد تایید را وارد کنید';
export const KAMPON_INFORMATION = 'مشخصات تخفیف';
export const KAMPONS = 'کمپُن ها';
export const SOLD_KAMPONS = 'کمپُن های فروخته شده';
export const BOUGHT_KAMPONS = 'کمپُن های خریداری شده';
export const USED_KAMPONS = 'کمپُن های استفاده شده';
export const ACTIVE_KAMPONS = 'کمپُن های  فعال';
export const BOOKMARKED = 'علاقه مندی ها';

export const LOGIN_TITLE = 'ورود به سفیرکارت';
export const MSG_SMT_WRONG = 'لطفا دوباره تلاش کنید';
export const INTERNET_ERROR_MSG = 'اتصال به اینترنت را بررسی کنید';
export const DATA_ERROR_MSG =
  'دریافت اطلاعات با مشکل مواجه شده است لطفا دوباره تلاش کنید';
export const NULL_ERROR_MSG = 'موردی برای نمایش یافت نشد';
export const PROFILE_LOGIN_MSG =
  'لطفا برای دیدن اطلاعات کاربری خود ابتدا ثبت نام کنید';
export const INPUT_PERSIAN_ERROR_MGS =
  'لطفا از حروف فارسی  و اعداد استفاده  کنید';
export const SUCCESS_REQ = 'درخواست با موفقیت ثبت شد';
export const DENIED_REQ = 'درخواست با خطا مواجه شد';
export const DENIED_LOGIN = 'لطفا برای ثبت گزارش ابتدا ثبت نام کنید';

export const WITHDRAW = 'WITHDRAW';
export const DEPOSIT = 'DEPOSIT';
//============== Error Codes==================
export const FAILED_NOT_OWNER = 1082;
export const FAILED_TOKEN = 1014;
export const FAILED_NETWORK = 1015;
export const NO_DISCOUNT_INCART = 1047;

export const DISCOUNT_IS_USED = 1094;
export const DISCOUNT_INVALID = 1093;
export const MEMBER_NOT_LOGIN = 1000;
export const NO_INTERNET = 1001;
export const SMT_WRONG = 1002;

//============== Null Errors ===========================
export const NULL_TRANSACTION = 9010;
export const EMAIL_ADDRESS = 'آدرس ایمیل';
export const ADDRESS_MEMBER = 'آدرس محل سکونت:';

export const NEW_PRICE = 'قیمت جدید:';
export const OLD_PRICE = 'قیمت قبلی:';
export const ADDRESS = 'آدرس:';
export const PHONES = 'شماره تماس:';
export const FEATURES = 'باشگاه ها ';

export const CART_ITEM = 'itemAdd';
export const TOKEN = 'token';
export const MOST_DISCOUNT = 'پرتخفیف ترین ها';
export const MOST_SOLD = 'پرفروش ترین ها';
export const POPULAR = 'محبوب ترین ها';
export const NEWEST = 'جدید ترین ها';
//================ Errors ==================
export const AUTH_FAILED = 1012;
export const DELETE_CART_FAILED = 1027;
export const CART_ITEM_NOT_EXIST = 1032;

//================ Response ==================

//============== Checks ===========================

//=============== CART ================
export const NO_ITEM_IN_CART = 'سبد خرید خالی است';
// ======================SPINNER ================
export const SPIN = 'چرخش گردونه';
export const TRY_YOUR_CHANSE = 'شانس خود را امتحان کتید ';
export const YOUR_REWARD = 'جایزه شما';
export const NEXT_SPIN = 'چرخونه بعدی تا :';
//========================  PROFILE ==================
export const USER_INFO = 'مشخصات کاربر';
export const MY_KAMPONS = 'کمپن های من';
export const MY_BALANCE = 'موجودی حساب من';
export const MY_TRANSACTIONS = 'تراکنش های من';
export const COMMON_QUESTIONS = 'سوالات متداول';
export const CONNECT_WITH_US = 'ارتباط با ما';
export const ABOUT_US = 'درباره سفیر کارت';
export const YOUR_MENTIONS = 'نظرات کاربران';
export const SUPPORT = 'پشتیبانی';
export const EXIT_USER = 'خروج';
export const TERM_OF_RULE = 'قوانین و مقررات';
//======================== HOME ITEMS ==================
export const MOST_VIEW = 'بیشترین بازدید ';
export const MOST_NEW = 'جدیدترین';
export const TODAY_OFFER = 'پیشنهاد امروز';
export const MOST_DISCOUNT_HOME = 'پر تخفیف ترین ها';
//=========================  FILTER ITEMS ==============
export const RASHT = 'رشت';
export const ASTANE = 'آستانه';
export const LAHIJAN = 'لاهیجان';
export const DIS_FILTER = 'تخفیف ها';
export const CLUB_FILTER = 'یاشگاه ها';
export const REMOVE_FILETR = 'حذف';
export const CITY = 'شهر';
export const TYPE = 'نوع';
export const FILTER_ACCORDING_CITY = 'فیلنر بر اساس شهر';
export const FILTER_ACCORDING_TYPE = 'فیلتر بر اساس نوع';

//============================== DR CLUBS =====================
export const DR_CLUBS_TITLE = 'باشگاه مشتریان دکتر کلابز';
export const SAFIR_CARD = 'سفیر کارت';
export const SEARCH_IN_SAFIRS = 'جستجو در مراکز سفیر کارت';
export const MOST_INSTALLMENT = 'بیشترین  اقساط';
export const COMMENT_TITLE = 'نوشتن نظر';
export const SUBMIT_TITLE = 'ثبت نظر';
export const NO_DATA = 'اطلاعاتی یافت نشد';
export const LOCATION_REQ =
  'ما برای نشان دادن فروشگاه های نزدیک شما نیاز به دسترسی موقعیت مکانی دستگاه شما را داریم آیا تایید میکنید؟';
//============================ UER INFORMATION  ==============
export const NAME = 'نام';
export const FAMILY = 'نام خانوادگی';
export const MELLI_CODE = 'کد ملی';
export const BORN_DATE = 'تاریخ تولد';
export const ORGANIZATION_NAME = 'نام سازمان';
export const TEL_NO = 'شماره تماس';
export const ADDRESS_USER = 'آدرس';
export const VALET_NO = 'شماره حساب بانک مهر ایران';
export const CARD_NO = 'شماره کارت اعتباری';
export const EDIT_USER_INFO = 'تغییر اطلاعات کاربری';
export const ENTER_CARD_NO = 'لطفا شماره کارت اعتباری خود را وارد کنید ';
//================================ LOGIN ========================
export const ENTER_11_NUmber = 'شماره  همراه ۱۱ رقمی خود را وارد کنید';
export const TRY_AGAIN = 'لطفا دوباره تلاش کنید';
export const ENTER_6_NUMBER = 'لطفا کد ۶ رقمی را کامل وارد کنید';
export const ENTERED_CODE_IS_WRONG = 'کد وارد شده اشتباه است';
export const EDIT_NUMBER = 'ویرایش شماره';
export const WRONG_MOBILE_MUMBER = 'شماره همراه اشتباه است';
//================================SPALSH ==========================
export const INSTALLMENT_NETWORK_TITILE = ' شبکه خرید وتخفیفات ';
export const FORSE_UPDATE_TITLE =
  'نسخه جدید برنامه منتشر شده است لطفا برای ادامه بروز رسانی کنید';
export const UPDATE_TITLE = 'نسخه جدید برنامه منتشر شده است';
//========================= VALIDATION CODES =========
export const UPDATE = 'بروز رسانی';
export const ENTER_YOUR_NAME = 'نام خود را وارد کنید ';
export const ENTER_YOUR_FAMILY = ' نام خانوادگی خود را وارد کنید ';
export const ENTER_MELLI_CODE = 'کد ملی ۱۰ رقمی خود را وارد کنید ';
export const CHANGE = 'ثبت';
export const AGREE_TERM = 'با قوانین و مقررات موافقم';
//================================COMMON QESTION =============
export const QES1 = 'به چه شکلی می توانم سفیر کارت تهیه کنم؟';
export const ANS1 =
  'پاسخ : در صورت کارمند بودن با مراجعه به واحد های رفاهی اداره خود و در غیر اینصورت با مراجعه به فروشگاه هایطرف قرارداد';
export const QES2 = 'آیا سفیر کارت به من که شغل آزاد دارم تعلق می گیرد؟';
export const ANS2 =
  'پاسخ : بله با توجه به اینکه تضامین بانکی اخذ می گردد و برای عموم قابل ارائه می باشد .';
export const QES3 = 'فروشگاه من چطور می تواند پذیرنده سفیر کارت شود ؟';
export const ANS3 =
  'پاسخ : با هماهنگی روابط عمومی شرکت آتیه پردازان می توانید به عنوان پذیرنده سفیر کارت باشید .';
export const QES4 = 'سقف تسهیالت پرداختی سفیرکارت چه مبلغی است؟';
export const ANS4 = 'پاسخ : تا مبلغ 000.000.200 ریال می باشد .';
export const QES5 = 'روش پرداخت اقساط سفیر کارت را توضیح دهید؟';
export const ANS5 = 'پاسخ : بصورت اقساط 12 ماهه با کارمزد 4 %می باشد';
export const QES6 =
  'آیا سفیر کارت با فروشگاه ها ی شهرستان ها نیز قرارداد منعقد می نماید ؟';
export const ANS6 =
  'پاسخ : بله در حال رایزنی با فروشگاههای شهرستان ها ی استان گیالن می باشیم';
export const QES7 = 'آیا با بیمارستان ها قرارداد منعقد می گردد';
export const ANS7 = 'پاسخ : در حال مذاکره هستیم';
export const QES8 = 'آیا با تاالرها و رستوران ها قرارداد منعقد می گردد:';
export const ANS8 = 'پاسخ : بله در حال رایزنی می باشیم';
//=========================== COMMENT =====================
export const SEND_COMMENT_TITLE =
  'پیام شما ارسال شد بعد از بررسی منتشر خواهد شد';
