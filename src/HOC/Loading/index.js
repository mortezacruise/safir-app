import React from 'react';
import {View, Text} from 'react-native';
import {Flex, centerAll} from '../../values/Theme';
function Loading(Component) {
  return function LoadingComponent({isLoading, ...props}) {
    if (!isLoading) return <Component {...props} />;
    else
      return (
        <View style={[Flex, centerAll]}>
          <Text>`error :${props.error}`</Text>
        </View>
      );
  };
}

export default Loading;
