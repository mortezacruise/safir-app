import React, {useEffect, useContext} from 'react';
import {View, Text} from 'react-native';
import {iranSans, Flex, pad4, pad8, Tac, h3, h4} from '../../values/Theme';
import {Context as DataContext} from '../../context/DataContext';

const TermOfUse = () => {
  const context = useContext(DataContext);
  const {privacy} = context.state;
  return (
    <View style={[Flex, pad8]}>
      <Text style={[Tac, iranSans, h3, {marginVertical: 16}]}>
        قوانین و مقررات
      </Text>
      <Text style={[iranSans, h4]}>{privacy}</Text>
    </View>
  );
};
export default TermOfUse;
