import React from 'react';
import {Text, TouchableWithoutFeedback, View} from 'react-native';
import {bgWhite, opacityText} from '../../values/Theme';
import styles from './styles';
import FastImage from 'react-native-fast-image';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {navigate} from '../../../navigationRef';

const ClubsRow = ({items}) => {
  const {item} = items;

  const onPress = () => {
    navigate('ClubsScreen', {clubParams: item});
  };
  const Thumnail = {uri: item.slides[0]};

  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={styles.discountContainer}>
        <FastImage style={styles.discountImage} source={Thumnail} />
        <View style={styles.Content}>
          <View style={styles.RightContent}>
            <Text
              numberOfLines={1}
              ellipsizeMode="tail"
              style={[styles.Text, opacityText, {textAlign: 'right'}]}>
              {item.title}
            </Text>
            <Text style={[styles.Text, opacityText, {marginBottom: 4}]}>
              کارمزد: {item.commission}
            </Text>
          </View>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default ClubsRow;
{
  /* <View style={styles.RatingHolder}>
<Rating
  startingValue={2}
  type="star"
  imageSize={15}
  ratingCount={5}
  defaultRating={1}
  readonly={true}
  style={styles.Stars}
/>
</View> */
}
