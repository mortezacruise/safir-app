import React from 'react';
import {
  Animated,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
  View,
  ImageBackground,
} from 'react-native';
import {
  bgWhite,
  s48,
  centerAll,
  posAbs,
  iranSans,
  h4,
  fWhite,
  h5,
  h6,
} from '../../values/Theme';
import styles from './styles';
import FastImage from 'react-native-fast-image';
import {Rating} from 'react-native-ratings';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {TOMAN} from '../../values/Strings';
import {navigate} from '../../../navigationRef';

const DiscountRow = ({items}) => {
  const {item} = items;
  const onPress = () => {
    navigate('ClubsScreen', {disVal: item});
  };
  const Thumnail = {uri: item.thumbnail};

  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={styles.discountContainer}>
        <ImageBackground
          style={[s48, centerAll, posAbs, {left: 6, zIndex: 1000}]}
          source={require('../../assets/Png/discount.png')}>
          <Text style={[iranSans, h4, fWhite]}>%10</Text>
        </ImageBackground>
        <View style={[bgWhite]}>
          <FastImage style={styles.discountImage} source={Thumnail} />
          <View style={styles.Content}>
            <View style={styles.RightContent}>
              <Text style={styles.Text}>{item.title}</Text>
              <Text style={[styles.Text, h6]}>{item.subTitle}</Text>
            </View>
            <View style={styles.Icons}>
              <View style={styles.PriceHolder}>
                <Text style={styles.Text}>
                  {item.newPrice} {TOMAN}
                </Text>
                <Text
                  style={[
                    styles.Text,
                    {color: 'red', textDecorationLine: 'line-through'},
                  ]}>
                  {item.realPrice}
                </Text>
              </View>

              <Icon />
            </View>
          </View>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default DiscountRow;
