import React, {useEffect} from 'react';
import {View, Text} from 'react-native';
import {iranSans} from '../../values/Theme';
const IransansText = (props) => {
  return <Text style={[iranSans, props.style]}>{props.children}</Text>;
};
export default IransansText;
