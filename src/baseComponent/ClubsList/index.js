import React, {useEffect} from 'react';
import {View, Text, FlatList} from 'react-native';
import ClubsRow from '../ClubsRow';
const ClubsList = (props) => {
  const renderItem = (items) => {
    return <ClubsRow items={items} />;
  };
  const keyExtractor = (item, index) => {
    return (Math.random() * 1.75 * Math.random() + index).toString();
  };
  return (
    <View>
      <FlatList
        showsHorizontalScrollIndicator={false}
        inverted
        horizontal
        data={props.data}
        renderItem={renderItem}
        keyExtractor={keyExtractor}
      />
    </View>
  );
};
export default ClubsList;
