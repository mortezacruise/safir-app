import React, {useEffect, useState} from 'react';
import {
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
  View,
  Image,
} from 'react-native';
import {screenWidth} from '../../values/Constants';
import styles from './styles';
import {opacityText} from '../../values/Theme';
import {TouchableOpacity} from 'react-native-gesture-handler';

const CategoryRow = (props) => {
  const onCategoryPress = (id) => {
    props.onCategoryPress(id);
  };
  const {image, titleFa} = props.cat;

  return (
    <TouchableOpacity onPress={() => onCategoryPress(props.cat._id)}>
      <View style={styles.categoryContainer}>
        <Image source={{uri: image}} style={[styles.Image]} />
        <Text style={[styles.categoryTitle, opacityText]}>{titleFa}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default CategoryRow;
const styless = StyleSheet.create({
  image: {
    width: screenWidth / 3,
    height: screenWidth / 5,
  },
});
