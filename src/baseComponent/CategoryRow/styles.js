import {StyleSheet} from 'react-native';
import {screenWidth} from '../../values/Constants';
import {iranSans, centerAll} from '../../values/Theme';
import {TEXT_COLOR, MAIN_COLOR} from '../../values/Colors';

export default styles = StyleSheet.create({
  categoryContainer: {
    marginHorizontal: 4,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
    borderRadius: 8,
    marginTop: 16,
    padding: 8,
  },

  categoryTitleParent: {
    alignItems: 'center',
  },

  categoryTitle: {
    fontSize: 12,
    ...iranSans,
    color: TEXT_COLOR,
  },

  Image: {
    width: screenWidth / 11,
    height: screenWidth / 11,
  },

  //for wraping category images
  imageView: {
    ...centerAll,
    width: screenWidth / 6,
    height: screenWidth / 6,
  },
});
