import React, {useEffect} from 'react';
import {View, Text, FlatList} from 'react-native';
import DiscountRow from '../DiscountRow';
const DisCountLists = ({data}) => {
  const renderItem = (items) => {
    return <DiscountRow items={items} />;
  };
  const keyExtractor = (item, index) => {
    return (Math.random() * 1.75 * Math.random() + index).toString();
  };
  return (
    <View style={{flex: 1}}>
      <FlatList
        showsHorizontalScrollIndicator={false}
        inverted
        horizontal
        data={data}
        renderItem={renderItem}
        keyExtractor={keyExtractor}
      />
    </View>
  );
};
export default DisCountLists;
