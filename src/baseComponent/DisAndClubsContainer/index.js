import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import DisCountLists from '../DisCountLists/index.js';
import ClubsList from '../ClubsList/index.js';
import {
  iranSans,
  h4,
  fMainColor,
  h6,
  h5,
  fRow,
  centerAll,
  mH8,
  opacityText,
  mH16,
} from '../../values/Theme.js';
import {TEXT_COLOR} from '../../values/Colors';
import {DIS_FILTER, CLUB_FILTER, TYPE} from '../../values/Strings';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {navigate} from '../../../navigationRef.js';
const DisAndClubsContainer = ({data, title, urlBody}) => {
  const {stores, discounts} = data;

  return (
    <View style={{flex: 1, width: '100%'}}>
      <View style={styles.TextView}>
        <Text style={[styles.Text, opacityText]}>{title}</Text>
        <TouchableOpacity
          style={[fRow, centerAll, mH16]}
          onPress={() => navigate('More', {data: urlBody})}>
          <Icon name="chevron-left" size={14} style={[opacityText]} />
          <Text style={[styles.MoreText, opacityText]}>بیشتر</Text>
        </TouchableOpacity>
      </View>
      <ClubsList data={stores} />
    </View>
  );
};
export default DisAndClubsContainer;
const styles = StyleSheet.create({
  Text: {
    ...iranSans,
    ...h5,
    marginRight: 16,
    color: TEXT_COLOR,
  },
  TextView: {
    flexDirection: 'row-reverse',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  MoreText: {
    ...iranSans,
    ...h6,
    marginLeft: 4,
    color: TEXT_COLOR,
  },
});
