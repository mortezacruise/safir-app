import React, {useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  Image,
  Dimensions,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from 'react-native';
import {LIGHT_GRAY, MAIN_COLOR} from '../../../values/Colors';
import {
  s48,
  centerAll,
  h4,
  iranSans,
  h5,
  h6,
  Tac,
  mV8,
} from '../../../values/Theme';
import {navigate} from '../../../../navigationRef';
import {cos} from 'react-native-reanimated';
import {NO_DATA} from '../../../values/Strings';
const {width, height} = Dimensions.get('screen');
const SearchList = ({data, title, navigation}) => {
  const onItemPresse = (item) => {
    navigate('ClubsScreen', {clubParams: item});
  };
  const renderItem = ({item}) => {
    return (
      <TouchableOpacity onPress={() => onItemPresse(item)}>
        <View style={styles.itemContainer}>
          <View style={styles.textHolder}>
            <Text style={[styles.itemText, h4]}>{item?.title || ''}</Text>
            <Text style={[styles.itemText, h6, {opacity: 0.5}]}>
              {item?.category?.titleFa || ''}
            </Text>
          </View>
          <Image
            source={{uri: item?.slides[0] || ''}}
            style={styles.itemImage}
          />
        </View>
      </TouchableOpacity>
    );
  };
  const keyExtractor = (item, index) => {
    return index.toString();
  };
  const items = () => {
    return (
      <FlatList
        data={data}
        renderItem={renderItem}
        keyExtractor={keyExtractor}
      />
    );
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>{title}</Text>
      <View style={styles.seprator} />
      {data && data.length < 1 ? (
        <Text style={[iranSans, Tac, mV8]}>{NO_DATA}</Text>
      ) : (
        items()
      )}
    </View>
  );
};
export default SearchList;
const styles = StyleSheet.create({
  container: {
    width: width - 50,
    justifyContent: 'center',
    margin: 8,
  },
  itemContainer: {
    flex: 1,
    width: width - 60,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    margin: 4,
    borderRadius: 16,
  },
  itemImage: {
    width: width / 6,
    height: width / 6,
    borderRadius: 100,
  },
  textHolder: {
    marginHorizontal: 16,
    alignItems: 'flex-end',
    justifyContent: 'flex-start',
    width: '60%',
    height: '100%',
    marginBottom: 16,
    paddingTop: 16,
  },
  seprator: {
    borderWidth: 1,
    borderColor: MAIN_COLOR,
    width: '100%',
    opacity: 0.7,
  },
  title: {
    textAlign: 'right',
    ...iranSans,
    ...h4,
    opacity: 0.7,
  },
  itemText: {
    ...iranSans,
  },
});
