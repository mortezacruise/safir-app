import React, { useState, useEffect } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import {
  bgGray,
  fRow,
  iranSans,
  h5,
  height13,
  fMainColor,
  fBlack,
  halfScrenw,
  centerAll,
  bBWidth,
} from "../../../../../Values/Theme";
import {
  SOLD_KAMPON,
  UN_SOLD_KAMPON,
  ACTIVE_KAMPONS,
  USED_KAMPONS,
} from "../../../../../Values/Strings";
const Tabs = (props) => {
  const [state, setState] = useState({ isActive: true });

  useEffect(() => {
    deActivePressed();
  }, []);

  const activePressed = () => {
    setState({ isActive: true });
    props.onTabClick(true);
  };
  const deActivePressed = () => {
    setState({ isActive: false });
    props.onTabClick(false);
  };

  const { isActive } = state;

  return (
    <View style={[height13]}>
      <View style={[fRow]}>
        <TouchableOpacity
          style={[halfScrenw, bgGray, height13, centerAll, isActive ? bBWidth : null]}
          onPress={activePressed}
        >
          <Text style={[iranSans, h5, isActive ? fMainColor : fBlack]}>
            {props.isActive ? USED_KAMPONS : ACTIVE_KAMPONS}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[halfScrenw, bgGray, height13, centerAll, isActive ? null : bBWidth]}
          onPress={deActivePressed}
        >
          <Text style={[iranSans, h5, isActive ? fBlack : fMainColor]}>
            {props.isActive ? ACTIVE_KAMPONS : USED_KAMPONS}
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
export default Tabs;
