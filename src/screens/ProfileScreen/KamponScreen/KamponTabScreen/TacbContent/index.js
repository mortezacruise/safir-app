import React, { Component, useState, useEffect, useContext } from "react";
import { ContentList } from "../ContenetList";
import { Context as UserContext } from "../../../../../Context/userDataContext";
const TabsContent = (props) => {
  const context = useContext(UserContext);
  const userState = context.state;
  const { discounts } = userState.userInfo;
  console.log({ kk: discounts });

  const [state, setState] = useState({
    data: [],
    isLoading: true,
    UsedItems: [],
    ActtiveItems: [],
    filteredData: [],
  });
  useEffect(() => {
    if (props.isActive) {
      getDeactiveKampons();
    } else {
      getActiveKampons();
    }
  }, [props.isActive]);
  const getActiveKampons = () => {
    const UsedItems = discounts.filter((data) => {
      return (data = data.isUsed === false);
    });
    setState({ ...state, filteredData: UsedItems });
  };
  const getDeactiveKampons = () => {
    const UnUsedItems = discounts.filter((data) => {
      return (data = data.isUsed === true);
    });
    setState({ ...state, filteredData: UnUsedItems });
  };

  return <ContentList data={state.filteredData} />;
};

export default TabsContent;
