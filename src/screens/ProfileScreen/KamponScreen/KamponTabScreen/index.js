import React, { Component, useState } from "react";
import { View, TouchableOpacity, Text } from "react-native";

import { iranSans, h5, Tac, h4, pad8, Flex, bgRed, bgGray } from "../../../../Values/Theme";
import Tabs from "./Tabs";
import TacbContent from "./TacbContent";

const KamponTabScreen = () => {
  const [state, setState] = useState({ isActive: true });
  const onTabclick = (isActive) => {
    setState({ isActive });
  };
  return (
    <View style={[Flex]}>
      <Tabs isActive={true} onTabClick={onTabclick} />
      <TacbContent isActive={state.isActive} />
    </View>
  );
};
export default KamponTabScreen;
