import {StyleSheet, Dimensions} from 'react-native';
const {height, width} = Dimensions.get('screen');
import {mainColor} from '../../../values/Colors';

const Styles = StyleSheet.create({
  Container: {
    flex: 1,
  },
  TopBack: {
    height: width,
    backgroundColor: mainColor,
    borderBottomStartRadius: 50,
    borderBottomEndRadius: 50,
  },
  Text: {
    fontFamily: 'iran_sans',
    fontSize: 20,
    textAlign: 'center',
    marginTop: 10,
    paddingHorizontal: 10,
  },

  ViewCenter: {
    position: 'absolute',
    width: width / 1.1,
    height: width * 1.65,
    backgroundColor: '#e8e8e8',
    borderRadius: 25,
    alignSelf: 'center',
    top: width / 7,
  },

  Number: {
    flexDirection: 'row',
    paddingVertical: 20,
    paddingHorizontal: 15,
  },
  TextInput: {
    fontFamily: 'iran_sans',
    paddingHorizontal: 10,
    marginTop: 10,
    textAlign: 'center',
  },
});
export default Styles;
