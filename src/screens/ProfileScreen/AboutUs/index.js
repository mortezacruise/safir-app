import React, {useState, useEffect} from 'react';
import {View, Text, Image, TouchableOpacity, ScrollView} from 'react-native';
import Styles from './styles';
const AboutUs = ({navigation}) => {
  const text4 =
    'در راستای حمایت از تولید و سرمایه ایرانی و به منظور تحقق منویات مقام معظم رهبری(مدظله) در خصوص سیاست های اقتصاد مقاومتی، شرکت آتیه پردازان سفیر گیل با مشارکت بانک قرض الحسنه مهر ایران، اقدام به طراحی و توزیع کارت اعتباری خرید کالای ایرانی به صورت اقساطی در بین اقشار مختلف جامعه نموده است.این موضوع خلاف شرع و طبق اعتقادات، باعث از بین رفتن برکات از زندگی می شود .  شرکت آتیه پردازان سفیر گیل با ایجاد شبکه فروش اقساطی کالا(بدون کارمزد)، اقدام به حذف این منکر از جامعه نموده است،امیدواریم با مدد الهی و همکاری تولیدکنندگان محترم، بتوانیم در این راه قدمهای استواری برداریم.';
  const titleText = 'درباره  سفیر کـارت';
  const endText = 'مدیریت هوشمند مشتریان دکتر کلابز';
  return (
    <View style={Styles.Container}>
      <View style={Styles.TopBack}></View>
      <ScrollView style={Styles.ViewCenter}>
        <Text style={Styles.Text}>{titleText}</Text>

        <Text style={Styles.TextInput}>{text4}</Text>
      </ScrollView>
    </View>
  );
};

export default AboutUs;
