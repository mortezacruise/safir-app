import React from 'react';
import {
  Text,
  TouchableWithoutFeedback,
  View,
  ImageBackground,
  ActivityIndicator,
  Image,
} from 'react-native';
import styles from './styles';
import {
  iranSans,
  h4,
  h5,
  bgMainColor,
  s48,
  centerAll,
  fWhite,
  h3,
  s32,
  posAbs,
  h6,
} from '../../../../values/Theme';
import {screenWidth} from '../../../../values/Constants';
import {navigate} from '../../../../../navigationRef';
export default SearchRow = ({searchItem, onItemSearchClick}) => {
  const onPressItem = () => {
    navigate('ClubsScreen', {clubParams: searchItem});
  };

  const disAvatar = searchItem.slides[0];
  const {title, subTitle, newPrice, realPrice} = searchItem;

  const percent = Math.floor(100 - (newPrice * 100) / realPrice);

  return (
    <TouchableWithoutFeedback onPress={onPressItem}>
      <View style={styles.searchRowContainer}>
        <Image style={styles.searchAvatar} source={{uri: disAvatar}} />
        <View style={styles.titleContainer}>
          <Text
            numberOfLines={1}
            ellipsizeMode="tail"
            style={[iranSans, h5, {width: screenWidth / 2.5}]}>
            {searchItem.title}
          </Text>
          <Text
            style={[
              iranSans,
              h6,
              {
                width: screenWidth / 2.5,
              },
            ]}
            ellipsizeMode="tail"
            numberOfLines={1}>
            اقساط {searchItem.installments} ماهه
          </Text>

          <Text
            style={[iranSans, h6, {width: '100%'}]}
            ellipsizeMode="tail"
            numberOfLines={1}>
            {searchItem.owner.address}
          </Text>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};
