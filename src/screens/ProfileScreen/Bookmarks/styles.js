import { StyleSheet } from "react-native";
import { MAIN_COLOR } from "../../../Values/Colors";
import { iranSans } from "../../../Values/Theme";

export default styles = StyleSheet.create({
  hamburger: {
    zIndex: 2,
    paddingBottom: 16,
    paddingRight: 16,
    paddingTop: 8,
    paddingLeft: 8,
    borderBottomRightRadius: 100,
    backgroundColor: "#50C3C6",
  },
  header: {
    flexDirection: "row",
    alignItems: "center",
  },
  headerText: {
    fontSize: 18,
    marginHorizontal: 40,
  },

  TabContent: {
    alignItems: "center",
    justifyContent: "center",
  },

  Pack: {
    backgroundColor: "white",
    borderRadius: 5,
    width: "45%",
    marginTop: 10,
    marginHorizontal: 9,
    borderWidth: 2,
    borderColor: MAIN_COLOR,
  },
  img: {
    alignSelf: "center",
    width: "100%",
    height: "50%",
    paddingVertical: 85,
    margin: 5,
  },
  TextTitle: {
    fontFamily: "iran_sans",
    fontSize: 18,
    borderTopWidth: 2,
    width: 100,
    textAlign: "center",
    borderColor: "#e8e8e8",
    color: "#d4af37",
  },
  TextDescription: {
    ...iranSans,
    fontSize: 10,
    flexDirection: "column",
  },
});
