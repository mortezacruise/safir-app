import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  StyleSheet,
  Image,
} from 'react-native';
import {useContext} from 'react';
import {Context as UserContxt} from '../../../context/userDataContext';
import {mV8, Flex, centerAll, iranSans, Tac, h3} from '../../../values/Theme';
import SearchRow from './SearchRow';
import {withNavigationFocus} from 'react-navigation';

const Bookmarks = (props) => {
  //=================== CONTEXT ======================
  const context = useContext(UserContxt);
  const {state, getMemberInformation} = context;
  const {userInfo} = state;
  const clubBook = userInfo?.storeBookmarks;

  //=================== CONTEXT ======================
  const [data, setData] = useState([]);
  //=================== CONTEXT ======================
  useEffect(() => {
    getMemberInformation();
  }, []);
  useEffect(() => {
    userInfo && setData(clubBook);
  }, [userInfo]);
  const renderlist = () => {
    if (data) {
      if (data.length < 1) {
        return (
          <View style={[Flex, centerAll]}>
            <Text style={[iranSans]}>اطلاعاتی یافت نشد</Text>
          </View>
        );
      }
    } else {
      return (
        <View style={[Flex, centerAll]}>
          <Text style={[iranSans]}>اطلاعاتی یافت نشد</Text>
        </View>
      );
    }

    return (
      <FlatList
        initialNumToRender={4}
        keyExtractor={(item) => {
          return item._id;
        }}
        data={data}
        renderItem={({item}) => {
          return <SearchRow searchItem={item} />;
        }}
      />
    );
  };

  return (
    <View style={[Flex, mV8]}>
      <Text
        style={[
          Tac,
          h3,
          iranSans,
          {borderBottomWidth: 2, borderBottomColor: '#d7d7d7'},
        ]}>
        فروشگاه های منتخب شما
      </Text>
      {renderlist()}
    </View>
  );
};

export default withNavigationFocus(Bookmarks);
