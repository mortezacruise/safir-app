import React, {useEffect, useContext} from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  FlatList,
  Image,
  Linking,
} from 'react-native';
import Styles from './styles';
import Icon from 'react-native-vector-icons/FontAwesome5';
import RNRestart from 'react-native-restart';
import {
  USER_INFO,
  MY_KAMPONS,
  MY_TRANSACTIONS,
  TERM_OF_USE,
  COMMON_QUESTIONS,
  CONNECT_WITH_US,
  ABOUT_US,
  EXIT_USER,
  YOUR_MENTIONS,
  SUPPORT,
  MY_BALANCE,
  TERM_OF_RULE,
} from '../../values/Strings';
import avatar from '../../assets/Png/Logo.png';
import {navigate} from '../../../navigationRef';
import {Context as UserDataContext} from '../../context/userDataContext';
import {iranSans, h3, fWhite} from '../../values/Theme';
import AsyncStorage from '@react-native-community/async-storage';
const ProfileScreen = () => {
  const context = useContext(UserDataContext);
  const {state, getMemberInformation} = context;
  useEffect(() => {
    getMemberInformation();
  }, []);
  const data = [
    {iconName: 'user', title: USER_INFO, route: 'UserInfo'},
    // {iconName: 'chart-bar', title: MY_TRANSACTIONS, route: 'TransAction'},
    // {iconName: 'calendar-day', title: MY_BALANCE, route: 'KamponScreen'},
    {iconName: 'book-open', title: TERM_OF_RULE, route: 'TermOfUse'},
    {iconName: 'user', title: COMMON_QUESTIONS, route: 'CommonQuestion'},
    {iconName: 'teamspeak', title: CONNECT_WITH_US, route: 'ContactUs'},
    {iconName: 'user', title: ABOUT_US, route: 'AboutUs'},
    {iconName: 'user-check', title: SUPPORT, route: 'ContactUs'},
    {iconName: 'sign-in-alt', title: EXIT_USER, route: 'exit'},
  ];
  const lastindex = data.length;

  // if (state.userInfo) {
  //   avatar = {uri: state.userInfo.avatar};
  // }
  const gotoBazar = async () => {
    Linking.openURL('https://cafebazaar.ir/app/ir.kampon.www.kampon/?l=fa');
    // CafeBazaarIntents.showRatePackage("ir.kampon.www.kampon").catch(() => {
    //   Linking.openURL("https://cafebazaar.ir/app/ir.kampon.www.kampon/?l=fa");
    // });
  };
  const onPressMiddle = (route) => {
    if (route === 'mention') {
      gotoBazar();
    } else {
      navigate(route);
    }
  };
  const onExitPress = () => {
    AsyncStorage.removeItem('token', () => RNRestart.Restart());
  };
  const renderItem = ({item, index}) => {
    if (index === 0) {
      return (
        <TouchableOpacity
          onPress={() => navigate(item.route)}
          style={Styles.firsRows}>
          <Icon name={item.iconName} size={30} color="#aaa" />
          <Text style={Styles.text}>{item.title}</Text>
        </TouchableOpacity>
      );
    } else if (index === lastindex - 1) {
      return (
        <TouchableOpacity onPress={onExitPress} style={Styles.lastRows}>
          <Icon name={item.iconName} size={30} color="#aaa" />
          <Text style={Styles.text}>{item.title}</Text>
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity
          onPress={() => onPressMiddle(item.route)}
          style={Styles.rows}>
          <Icon name={item.iconName} size={30} color="#aaa" />
          <Text style={Styles.text}>{item.title}</Text>
        </TouchableOpacity>
      );
    }
  };
  return (
    <ScrollView
      showsVerticalScrollIndicator={false}
      scrollEventThrottle={1}
      renderToHardwareTextureAndroid={true}>
      <View style={Styles.container}>
        <View style={Styles.topView}>
          <Image source={avatar} style={Styles.image} />
          <Text style={[iranSans, h3, fWhite]}>
            {state.userInfo?.name || 'ناشناس'}
          </Text>
        </View>
        <View style={Styles.mainView}>
          <FlatList
            data={data}
            renderItem={renderItem}
            keyExtractor={(item, index) => item.title}
          />
        </View>
      </View>
    </ScrollView>
  );
};
export default ProfileScreen;
