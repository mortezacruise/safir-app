import React from 'react';
import {Text, TouchableWithoutFeedback, View} from 'react-native';
import {DEPOSIT} from '../../../../values/Strings';
import {
  bgGray,
  bgGray600,
  bgMainColor,
  center,
  el1,
  fBlack,
  fRow,
  fWhite,
  h2,
  h3,
  h4,
  h5,
  height001,
  iranSans,
  m4,
  m8,
  mH16,
  pad10,
  r8,
  spaceB,
  Tac,
  tR8,
  h6,
} from '../../../../values/Theme';

const TransactionRow = ({transaction}) => {
  const {otType, otTime, otDescription, _id} = transaction;
  let {otAmount} = transaction;
  if (otAmount.toString().charAt(0) === '-')
    otAmount = parseInt(otAmount.toString().substring(1), 10);
  // const iranianAmount = new irAmount(otAmount).digitGrouped();
  if (otType === DEPOSIT) {
    return (
      <TouchableWithoutFeedback>
        <View style={[r8, el1, m8, mH16]}>
          <View style={[fRow, tR8, spaceB, center, bgMainColor]}>
            <Text style={[h5, iranSans, m8, fWhite]}>واریز به حساب</Text>
            <View style={[fRow, m4]}>
              <Text style={[h5, iranSans, m4, fWhite]}> {otTime}</Text>
            </View>
          </View>
          <View style={[height001, bgGray]} />
          <View style={[pad10]}>
            <Text style={[h5, fBlack, iranSans]}> {otDescription}</Text>
            <Text style={[iranSans, h5]}>قیمت : {iranianAmount} تومان</Text>
          </View>
          <View style={[height001, bgGray, m4]} />
          <Text style={[Tac, h6, iranSans]}>شماره پیگیری :{_id}</Text>
        </View>
      </TouchableWithoutFeedback>
    );
  } else {
    return (
      <TouchableWithoutFeedback>
        <View style={[r8, el1, m8, mH16]}>
          <View style={[fRow, tR8, spaceB, center, bgGray600]}>
            <Text style={[h5, iranSans, m8, fWhite]}>برداشت از حساب</Text>
            <View style={[fRow, m4]}>
              <Text style={[h5, iranSans, m4, fWhite]}> {otTime}</Text>
            </View>
          </View>
          <View style={[height001, bgGray]} />
          <View style={[pad10]}>
            <Text style={[h4, fBlack, iranSans]}> {otDescription}</Text>
            <Text style={[iranSans, h5]}>قیمت : {iranianAmount} تومان</Text>
          </View>
          <View style={[height001, bgGray, m4]} />
          <Text style={[Tac, h6, iranSans]}>شماره پیگیری :{_id}</Text>
        </View>
      </TouchableWithoutFeedback>
    );
  }
};
export default TransactionRow;
