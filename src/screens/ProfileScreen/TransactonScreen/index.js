import React, {Component, useState} from 'react';
import {View, Text} from 'react-native';
import {
  iranSans,
  m16,
  h3,
  tCenter,
  Flex,
  h1,
  fBlack,
  mV8,
  mV16,
} from '../../../values/Theme';
import TransactionContainer from './TransactionContainer';
const TransactionScreen = () => {
  const [isRefresh, setIsRefresh] = useState(false);

  return (
    <View style={Flex}>
      <Text style={[iranSans, h1, fBlack, mV16, tCenter, m16]}>
        گزارش های حسابداری
      </Text>
      <TransactionContainer isRefresh={isRefresh} />
    </View>
  );
};
export default TransactionScreen;
