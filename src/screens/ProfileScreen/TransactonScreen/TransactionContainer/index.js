import React, {Component, useState} from 'react';
import {ToastAndroid, ActivityIndicator, View} from 'react-native';
import TransactionList from '../TransactionList';

import {Indicator} from '../../../../components/Indicator';

const TransactionContainer = () => {
  const [state, setState] = useState({
    transactions: [],
    isNullTransaction: true,
    isLoading: false,
  });
  if (state.isLoading) return <Indicator />;
  else {
    if (state.isNullTransaction) return <View />;
    return <TransactionList transactions={state.transactions} />;
  }
};
export default TransactionContainer;
