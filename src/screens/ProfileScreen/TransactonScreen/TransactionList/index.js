import React from 'react';
import {FlatList} from 'react-native';
import TransactionRow from '../TransactionRow';

const TransactionList = ({transactions}) => {
  const renderTransaction = (transaction) => {
    // return <TransactionRow transaction={transaction} />;
  };

  return (
    <FlatList
      data={transactions}
      renderItem={({item}) => renderTransaction(item)}
      keyExtractor={(item, index) => index.toString()}
    />
  );
};

export default TransactionList;
