import React, {useEffect} from 'react';
import {View, Text} from 'react-native';
import {Flex, pad8} from '../../../values/Theme';
import IransansText from '../../../baseComponent/IransanText';
import {
  QES1,
  ANS1,
  QES2,
  ANS2,
  QES3,
  ANS3,
  QES4,
  ANS4,
  QES5,
  ANS5,
  QES6,
  ANS6,
  QES7,
  ANS7,
  QES8,
  ANS8,
} from '../../../values/Strings';
const CommonQuestion = () => {
  return (
    <View style={[Flex, pad8]}>
      <IransansText style={{textAlign: 'right'}}>{QES1}</IransansText>
      <IransansText style={{textAlign: 'right'}}>{ANS1}</IransansText>
      <IransansText style={{textAlign: 'right'}}>{QES2}</IransansText>
      <IransansText style={{textAlign: 'right'}}>{ANS2}</IransansText>
      <IransansText style={{textAlign: 'right'}}>{QES3}</IransansText>
      <IransansText style={{textAlign: 'right'}}>{ANS3}</IransansText>
      <IransansText style={{textAlign: 'right'}}>{QES4}</IransansText>
      <IransansText style={{textAlign: 'right'}}>{ANS4}</IransansText>
      <IransansText style={{textAlign: 'right'}}>{QES5}</IransansText>
      <IransansText style={{textAlign: 'right'}}>{ANS5}</IransansText>
      <IransansText style={{textAlign: 'right'}}>{QES6}</IransansText>
      <IransansText style={{textAlign: 'right'}}>{ANS6}</IransansText>
      <IransansText style={{textAlign: 'right'}}>{QES7}</IransansText>
      <IransansText style={{textAlign: 'right'}}>{ANS7}</IransansText>
      <IransansText style={{textAlign: 'right'}}>{QES8}</IransansText>
      <IransansText style={{textAlign: 'right'}}>{ANS8}</IransansText>
    </View>
  );
};
export default CommonQuestion;
