import React from 'react';
import {Button} from 'react-native-elements';
import {MAIN_COLOR} from '../../../../values/Colors';

const FormButton = ({title, buttonType, buttonColor, ...rest}) => (
  <Button
    {...rest}
    type={buttonType}
    title={title}
    buttonStyle={{borderColor: MAIN_COLOR, borderRadius: 20, minWidth: 100}}
    titleStyle={{color: MAIN_COLOR}}
  />
);

export default FormButton;
