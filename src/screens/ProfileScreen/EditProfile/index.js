import React, {Fragment, useContext} from 'react';
import {StyleSheet, SafeAreaView, View, ScrollView} from 'react-native';
import {Formik} from 'formik';
import * as Yup from 'yup';
import FormInput from './FormInput';
import FormButton from './FormButton';
import ErrorMessage from './ErrorMessage';
import {
  EMAIL_ADDRESS,
  NAME,
  FAMILY,
  MELLI_CODE,
  BORN_DATE,
  ORGANIZATION_NAME,
  TEL_NO,
  ADDRESS_USER,
  VALET_NO,
  CARD_NO,
  ENTER_YOUR_NAME,
  ENTER_YOUR_FAMILY,
  ENTER_MELLI_CODE,
  CHANGE,
  ENTER_CARD_NO,
} from '../../../values/Strings';
import {centerAll, padV8} from '../../../values/Theme';
import {Context} from '../../../context/userDataContext';
const validationSchema = Yup.object().shape({
  name: Yup.string().label('name').required(ENTER_YOUR_NAME),
  family: Yup.string().label('family').required(ENTER_YOUR_FAMILY),
  melliCode: Yup.string()
    .label('melliCode')
    .required(ENTER_MELLI_CODE)
    .min(10, ENTER_MELLI_CODE)
    .max(10, ENTER_MELLI_CODE),
  cardNo: Yup.string().label('cardNo').required(ENTER_CARD_NO),
});

const EditProfile = (props) => {
  const context = useContext(Context);
  const {state, putUserInfo} = context;
  const handleSubmit = async (values) => {
    putUserInfo(values, () => props.navigation.pop());
  };

  const {
    address,
    avatar,
    bankAccountNumber,
    cardNumber,
    email,
    familyName,
    idCardNumber,
    name,
    organName,
    phoneNumber,
  } = state.userInfo;
  return (
    <SafeAreaView style={styles.container}>
      <Formik
        initialValues={{
          name: name,
          family: familyName,
          melliCode: idCardNumber,
          organizationName: organName,
          telNumber: phoneNumber,
          address: address,
          valetNo: bankAccountNumber,
          cardNo: cardNumber,
        }}
        onSubmit={(values) => {
          handleSubmit(values);
        }}
        validationSchema={validationSchema}>
        {({
          handleChange,
          values,
          handleSubmit,
          errors,
          isValid,
          touched,
          handleBlur,
          isSubmitting,
        }) => (
          <ScrollView contentContainerStyle={[centerAll, padV8]}>
            <FormInput
              name="name"
              value={values.name}
              onChangeText={handleChange('name')}
              placeholder={NAME}
              iconName="user"
              iconColor="#2C384A"
              onBlur={handleBlur('name')}
              autoFocus
            />
            <ErrorMessage errorValue={touched.name && errors.name} />
            <FormInput
              name="family"
              value={values.family}
              onChangeText={handleChange('family')}
              placeholder={FAMILY}
              iconName="users"
              iconColor="#2C384A"
              onBlur={handleBlur('family')}
            />
            <ErrorMessage errorValue={touched.family && errors.family} />
            <FormInput
              name="melliCode"
              value={values.melliCode}
              onChangeText={handleChange('melliCode')}
              placeholder={MELLI_CODE}
              iconName="id-card"
              iconColor="#2C384A"
              onBlur={handleBlur('melliCode')}
              keyboardType={'numeric'}
            />
            <ErrorMessage errorValue={touched.melliCode && errors.melliCode} />

            <FormInput
              name="organizationName"
              value={values.organizationName}
              onChangeText={handleChange('organizationName')}
              placeholder={ORGANIZATION_NAME}
              iconName="city"
              iconColor="#2C384A"
              onBlur={handleBlur('organizationName')}
            />
            <ErrorMessage
              errorValue={touched.organizationName && errors.organizationName}
            />
            <FormInput
              name="telNumber"
              value={values.telNumber}
              onChangeText={handleChange('telNumber')}
              placeholder={TEL_NO}
              iconName="phone"
              iconColor="#2C384A"
              onBlur={handleBlur('telNumber')}
              keyboardType={'numeric'}
            />
            <ErrorMessage errorValue={touched.telNumber && errors.telNumber} />
            <FormInput
              name="address"
              value={values.address}
              onChangeText={handleChange('address')}
              placeholder={ADDRESS_USER}
              iconName="search-location"
              iconColor="#2C384A"
              onBlur={handleBlur('address')}
            />
            <ErrorMessage errorValue={touched.address && errors.address} />
            <FormInput
              name="valet"
              value={values.valetNo}
              onChangeText={handleChange('valetNo')}
              placeholder={VALET_NO}
              iconName="credit-card"
              iconColor="#2C384A"
              onBlur={handleBlur('valetNo')}
              keyboardType={'numeric'}
            />
            <ErrorMessage errorValue={touched.valetNo && errors.valetNo} />
            <FormInput
              name="cardNo"
              value={values.cardNo}
              onChangeText={handleChange('cardNo')}
              placeholder={CARD_NO}
              iconName="credit-card"
              iconColor="#2C384A"
              onBlur={handleBlur('cardNo')}
              keyboardType={'numeric'}
            />
            <ErrorMessage errorValue={touched.cardNo && errors.cardNo} />

            <View style={styles.buttonContainer}>
              <FormButton
                buttonType="outline"
                onPress={handleSubmit}
                title={CHANGE}
                buttonColor="#039BE5"
                disabled={!isValid || isSubmitting}
                loading={isSubmitting}
              />
            </View>
          </ScrollView>
        )}
      </Formik>
    </SafeAreaView>
  );
};
export default EditProfile;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  buttonContainer: {
    margin: 25,
  },
});
//   email: Yup.string()
//     .label('Email')
//     .email('Enter a valid email')
//     .required('Please enter a registered email'),
//   password: Yup.string()
//     .label('Password')
//     .required()
//     .min(4, 'Password must have more than 4 characters '),
