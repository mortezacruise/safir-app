import React, {memo} from 'react';
import {Input} from 'react-native-elements';
import {StyleSheet, View, Dimensions} from 'react-native';
import Ionicons from 'react-native-vector-icons/FontAwesome5';

const FormInput = ({
  iconName,
  iconColor,
  returnKeyType,
  keyboardType,
  name,
  placeholder,

  value,
  ...rest
}) => {
  return (
    <View style={styles.inputContainer}>
      <Input
        inputStyle={styles.input}
        inputContainerStyle={{backgroundColor: '#d7d7d7', borderRadius: 12}}
        {...rest}
        rightIcon={<Ionicons name={iconName} size={28} color={iconColor} />}
        rightIconContainerStyle={styles.iconStyle}
        placeholderTextColor="grey"
        name={name}
        value={value}
        placeholder={placeholder}
        style={styles.input}
        containerStyle={styles.inputContainerStyle}
        keyboardType={keyboardType || 'default'}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  inputContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 4,
  },
  iconStyle: {
    marginRight: 10,
  },
  input: {
    textAlign: 'center',
    width: 100,
    // marginVertical: 4,
    // marginHorizontal: 10,
  },
  inputContainerStyle: {
    width: Dimensions.get('screen').width - 100,
  },
});

export default memo(FormInput);
