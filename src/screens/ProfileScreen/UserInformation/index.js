import React, {useEffect, useContext} from 'react';
import {View, Text, ScrollView, TouchableOpacity} from 'react-native';
import styles from './styles';
import Spacer from '../../../components/Spacer';
import {
  NAME,
  FAMILY,
  BORN_DATE,
  ORGANIZATION_NAME,
  TEL_NO,
  ADDRESS_USER,
  VALET_NO,
  CARD_NO,
  MELLI_CODE,
  EDIT_USER_INFO,
} from '../../../values/Strings';
import {centerAll, fWhite} from '../../../values/Theme';

import {withNavigationFocus} from 'react-navigation';
import {Context} from '../../../context/userDataContext';
const UserInformation = ({navigation, isFocused}) => {
  const context = useContext(Context);
  const {state, getMemberInformation} = context;
  const {
    address,
    avatar,
    bankAccountNumber,
    cardNumber,
    email,
    familyName,
    idCardNumber,
    name,
    organName,
    phoneNumber,
  } = state.userInfo;
  useEffect(() => {
    isFocused && getMemberInformation();
  }, [isFocused]);
  const data = [
    {key: NAME, value: name},
    {key: FAMILY, value: familyName},
    {key: MELLI_CODE, value: idCardNumber},
    {key: ORGANIZATION_NAME, value: organName},
    {key: TEL_NO, value: phoneNumber},
    {key: ADDRESS_USER, value: address},
    {key: VALET_NO, value: bankAccountNumber},
    {key: CARD_NO, value: cardNumber},
  ];
  const row = () => {
    return data.map((item) => {
      return (
        <View style={styles.rowView}>
          <Text style={styles.text}>{item.key}:</Text>
          <Text
            style={styles.text}
            ellipsizeMode={'middle'}
            minimumFontScale={3}>
            {item.value}
          </Text>
        </View>
      );
    });
  };
  return (
    <View style={styles.container}>
      <View style={styles.mainContent}>
        <ScrollView
          showsVerticalScrollIndicator={true}
          contentContainerStyle={[centerAll, {padding: 12}]}>
          {row()}
        </ScrollView>
      </View>
      <TouchableOpacity
        onPress={() => navigation.push('EditProfile')}
        style={styles.button}>
        <Text style={[styles.text, fWhite]}>{EDIT_USER_INFO}</Text>
      </TouchableOpacity>
    </View>
  );
};
export default withNavigationFocus(UserInformation);
