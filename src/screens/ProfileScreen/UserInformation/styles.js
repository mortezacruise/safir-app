import {StyleSheet, Dimensions} from 'react-native';
import {iranSans, centerAll} from '../../../values/Theme';
import {MAIN_COLOR} from '../../../values/Colors';

const {width, height} = Dimensions.get('screen');
export default styles = StyleSheet.create({
  container: {
    flex: 1,
    // alignItems: 'flex-end',
    ...centerAll,
  },
  mainContent: {
    width: width - 50,
    height: height / 1.5,
    // backgroundColor: 'white',
    ...centerAll,
    borderRadius: 12,
  },
  text: {
    ...iranSans,
    marginRight: 12,
  },
  rowView: {
    maxWidth: '90%',
    minWidth: '90%',
    backgroundColor: 'gray',
    opacity: 0.7,
    borderRadius: 12,
    marginBottom: 8,
    padding: 8,
  },
  button: {
    width: width / 2,
    padding: 12,
    backgroundColor: MAIN_COLOR,
    borderRadius: 12,
    marginTop: 24,
    ...centerAll,
  },
});
