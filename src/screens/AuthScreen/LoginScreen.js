import React, {useState, useContext, useEffect, useRef} from 'react';
import {
  TextInput,
  Image,
  KeyboardAvoidingView,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  ToastAndroid,
  Dimensions,
  ActivityIndicator,
  StyleSheet,
} from 'react-native';
import evvelope from '../../assets/Png/Logo.png';

import {
  bgMainColor,
  centerAll,
  Flex,
  fWhite,
  h3,
  h4,
  iranSans,
  mH32,
  Tac,
  mV4,
  bgWhite,
  mH8,
  mV8,
  mV16,
  pad16,
  h5,
  fRow,
  h6,
  center,
} from '../../values/Theme';
import {Context as AuthContext} from '../../context/AuthContext';
import TokenScreen from './TokenScreen/TokenScreen';
import {screenHeight, screenWidth} from '../../values/Constants';
import {
  EDIT_NUMBER,
  ENTER_11_NUmber,
  WRONG_MOBILE_MUMBER,
  ENTER_6_NUMBER,
  AGREE_TERM,
} from '../../values/Strings';
import {MAIN_COLOR} from '../../values/Colors';
import {CheckBox} from 'native-base';

const mobilenumberlable = 'شماره همراه';
const entertoapptext = 'ورود';
const LodinScreen = ({navigation, H1, type}) => {
  const {state, signup, editNumber, clearErrorMessage} = useContext(
    AuthContext,
  );
  const {loading, errorMessage} = state;
  const [rightLable, setRightLable] = useState(false);
  const [inputValue, setInputValue] = useState('');
  const [checked, setChecked] = useState(false);
  let inputRef = useRef();
  useEffect(() => {
    inputRef?.focus();
  }, []);
  const pressCheckBox = () => {
    setChecked(!checked);
  };
  const Inputs = () => {
    switch (type) {
      case 'Login':
        return (
          <>
            <TextInput
              ref={(ref) => (inputRef = ref)}
              placeholder={'09 - -  - - -  - - - -'}
              maxLength={11}
              value={inputValue}
              onChangeText={_changeMobileInput}
              keyboardType={'numeric'}
              textAlignVertical={'center'}
              onBlur={() => setRightLable(false)}
              onFocus={() => setRightLable(true)}
              style={styles.textInput}
            />
            <TouchableOpacity
              disabled={!checked}
              onPress={() => signup(inputValue)}
              style={styles.button}>
              {loading ? (
                <ActivityIndicator size={40} color="white" />
              ) : (
                <Text style={[iranSans, fWhite, h4]}>{entertoapptext}</Text>
              )}
            </TouchableOpacity>

            {errorMessage ? (
              <Text
                style={[
                  iranSans,
                  h5,
                  mV8,
                  {color: 'red', textAlign: 'center'},
                ]}>
                {errorMessage}
              </Text>
            ) : null}
          </>
        );
      case 'Token':
        return (
          <TokenScreen mobileNumber={inputValue} navigation={navigation} />
        );
      default:
        return null;
    }
  };
  const toast = () => {
    ToastAndroid.show(WRONG_MOBILE_MUMBER, ToastAndroid.SHORT);
  };
  const _changeMobileInput = (text) => {
    let newText = '';
    let numbers = '0123456789';
    for (let i = 0; i < text.length; i++) {
      if (numbers.indexOf(text[i]) > -1) {
        if (text.charAt(0) != '0') return toast();
        if (text.length > 1 && text.charAt(1) != '9') return toast();

        newText = newText + text[i];
      } else return toast();
    }
    setInputValue(newText);
  };
  const overInputTitle = () => {
    return (
      <Text style={[styles.text, mV8]}>
        {type === 'Login' ? ENTER_11_NUmber : ENTER_6_NUMBER}
      </Text>
    );
  };
  return (
    <View style={[Flex, bgMainColor]}>
      <KeyboardAvoidingView style={[Flex]} behavior="position" enabled>
        <View style={[{height: screenHeight / 2}, bgMainColor, centerAll]}>
          <Image
            source={evvelope}
            style={[{width: screenWidth / 3, height: screenWidth / 3}]}
          />
          <Text style={[iranSans, h3, fWhite, Tac, mV16]}>{H1}</Text>
          {type === 'Token' ? (
            <Text style={[iranSans, mV8, fWhite]}>{inputValue}</Text>
          ) : null}
          {type === 'Token' ? (
            <TouchableOpacity
              onPress={() => {
                editNumber(), clearErrorMessage();
              }}>
              <Text
                style={[
                  iranSans,
                  h5,
                  fWhite,
                  {textDecorationLine: 'underline'},
                ]}>
                {EDIT_NUMBER}
              </Text>
            </TouchableOpacity>
          ) : null}
        </View>
        <View style={styles.lowerContent}>
          {overInputTitle()}
          {Inputs()}
          <View
            style={[
              fRow,
              centerAll,
              {
                width: '90%',
                marginTop: 12,
                padding: 4,
              },
            ]}>
            <CheckBox
              color={MAIN_COLOR}
              checked={checked}
              onPress={pressCheckBox}
              style={{marginEnd: 36}}
            />
            <TouchableOpacity onPress={() => navigation.push('TermOfUse')}>
              <Text
                style={{
                  textDecorationLine: 'underline',
                  marginStart: 12,
                }}>
                {AGREE_TERM}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAvoidingView>
    </View>
  );
};
LodinScreen.navigationOptions = () => {
  return {
    header: null,
  };
};
const styles = StyleSheet.create({
  text: {
    ...iranSans,
  },
  lowerContent: {
    ...bgWhite,
    ...centerAll,
    width: '100%',
    height: screenHeight / 2,
    borderTopLeftRadius: 40,
    borderTopRightRadius: 40,
  },
  textInput: {
    height: screenHeight / 12,
    width: screenWidth / 1.5,
    borderBottomWidth: 0,
    borderRadius: 20,
    direction: 'rtl',
    textAlign: 'center',
    ...pad16,
    ...iranSans,

    backgroundColor: 'rgba(64,199,133,0.2)',
  },
  button: {
    ...centerAll,

    alignSelf: 'center',
    borderRadius: 20,
    marginTop: screenWidth / 15,
    width: screenWidth / 2,
    backgroundColor: MAIN_COLOR,
    padding: 12,
  },
});
export default LodinScreen;
