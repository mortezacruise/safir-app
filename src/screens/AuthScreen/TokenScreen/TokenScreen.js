import React, {useEffect, useState, useContext} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {Context as AuthContext} from '../../../context/AuthContext';
import {Context as DataContext} from '../../../context/DataContext';
import {
  iranSans,
  h4,
  mV8,
  centerAll,
  fWhite,
  Flex,
  pad16,
  h5,
} from '../../../values/Theme';
import {screenWidth, screenHeight} from '../../../values/Constants';
import {MAIN_COLOR} from '../../../values/Colors';

const TokenScreen = (props) => {
  const [codeInputValue, setCodeInputValue] = useState('');
  const [retrySendMobile, setRetrySendMobile] = useState(false);
  const [time, setTimer] = useState(null);
  const {fetchSplashData} = useContext(DataContext);
  const {state, signup, enterToken, clearErrorMessage} = useContext(
    AuthContext,
  );
  const {loading, errorMessage} = state;
  const TimerContDown = async () => {
    let date = new Date();
    let time = date.getTime() + 122000;

    let countDownDate = new Date(time).getTime();

    let x = setInterval(async () => {
      let now = new Date().getTime();

      let distance = countDownDate - now;

      let days = Math.floor(distance / (1000 * 60 * 60 * 24));
      let hours = Math.floor(
        (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60),
      );
      let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      let seconds = Math.floor((distance % (1000 * 60)) / 1000);

      setTimer('  ارسال مجدد کد تا  ' + minutes + ':' + seconds);

      console.log(distance);

      if (distance < 1000) {
        console.log('tamam');

        clearInterval(x);
        setRetrySendMobile(true);
      }
    }, 1000);
  };
  useEffect(() => {
    TimerContDown();
  }, []);
  const resendCode = () => {
    return (
      <TouchableOpacity
        disabled={!retrySendMobile}
        onPress={() => {
          signup(props.mobileNumber);
          setRetrySendMobile((retrySendMobile) => !retrySendMobile);
        }}>
        {retrySendMobile ? (
          <Text style={[iranSans, h5]}>ارسال مجدد کد </Text>
        ) : (
          <Text style={[iranSans, h5]}>{time}</Text>
        )}
      </TouchableOpacity>
    );
  };
  const onChangeCodeInput = (text) => {
    setCodeInputValue(text);
  };
  return (
    <View style={[centerAll]}>
      <TextInput
        placeholder={'------'}
        maxLength={6}
        value={codeInputValue}
        onChangeText={onChangeCodeInput}
        keyboardType={'numeric'}
        textAlignVertical={'center'}
        style={[
          pad16,
          iranSans,
          {
            height: screenHeight / 12,
            width: screenWidth / 1.5,
            borderBottomWidth: 0,
            borderRadius: 20,
            direction: 'rtl',
            textAlign: 'center',
            backgroundColor: 'rgba(64,199,133,0.2)',
          },
        ]}
      />
      <TouchableOpacity
        onPress={() =>
          enterToken(codeInputValue, props.mobileNumber, () =>
            fetchSplashData(() => props.navigation.navigate('Tabs')),
          )
        }
        style={[
          centerAll,
          {
            alignSelf: 'center',
            borderRadius: 20,
            marginTop: screenWidth / 15,
            width: screenWidth / 2,
            height: 50,
            backgroundColor: MAIN_COLOR,
            padding: 12,
          },
        ]}>
        {loading ? (
          <ActivityIndicator size={40} color="white" />
        ) : (
          <Text style={[iranSans, fWhite, h4]}>{'تایید کد ارسالی'}</Text>
        )}
      </TouchableOpacity>
      {errorMessage ? (
        <Text style={[iranSans, h5, mV8, {color: 'red'}]}>{errorMessage}</Text>
      ) : null}
      <View style={[centerAll, mV8, {alignSelf: 'center'}]}>
        {resendCode()}
      </View>
    </View>
  );
};
export default TokenScreen;
