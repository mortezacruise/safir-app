import React, {useContext, useEffect, useState} from 'react';
import LoginScreen from './LoginScreen';
import {Context as AuthContext} from '../../context/AuthContext';
import {LOGIN_TITLE, CODE_TITLE} from '../../values/Strings';
import ErrorHandler from '../../HOC/Loading';
const AuthScreen = ({navigation}) => {
  const {state} = useContext(AuthContext);

  const interName = false;
  const enterToken = false;
  if (interName) {
    return null;
  } else if (state.showCode) {
    return <LoginScreen navigation={navigation} H1={CODE_TITLE} type="Token" />;
  } else {
    return (
      <LoginScreen navigation={navigation} H1={LOGIN_TITLE} type="Login" />
    );
  }
};
AuthScreen.navigationOptions = () => {
  return {
    header: null,
  };
};
export default AuthScreen;
