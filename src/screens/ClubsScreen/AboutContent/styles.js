import {StyleSheet} from 'react-native';
import {
  bgWhite,
  centerAll,
  Flex,
  fMainColor,
  fRow,
  h1,
  h2,
  h5,
  iranSans,
  pad8,
  posAbs,
  r8,
  fWhite,
  h4,
} from '../../../values/Theme';
const Styles = StyleSheet.create({
  Container: {
    ...Flex,
    // marginTop: 8,
    // backgroundColor: '#eee',
    marginBottom: 8,
    // elevation: 1,
    marginHorizontal: 8,
    overflow: 'hidden',
    borderRadius: 8,
  },
  Text: {
    ...iranSans,
    ...h5,
    textAlign: 'right',
  },
  Header: {
    ...fRow,
    justifyContent: 'flex-end',
    alignItems: 'center',
    padding: 16,
    elevation: 1,
    ...bgWhite,
    ...r8,
    marginTop: 8,
  },
  HeaderText: {
    ...h4,
    ...iranSans,
    // marginEnd: 8,
  },
  Icon: {
    ...posAbs,
    left: 10,
  },
  Accordion: {
    backgroundColor: 'transparent',
    marginBottom: 8,
    borderWidth: 0,
    borderColor: 'white',

    // cor
  },
  Content: {
    borderRadius: 8,
    paddingHorizontal: 8,
    justifyContent: 'space-between',
    alignItems: 'center',
    // overflow: 'hidden',
    elevation: 1,
  },
});
export default Styles;
