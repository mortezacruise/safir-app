import {StyleSheet, Dimensions} from 'react-native';
import {MAIN_COLOR} from '../../../../values/Colors';
import {iranSans, centerAll} from '../../../../values/Theme';
const {width, height} = Dimensions.get('screen');
const Styles = StyleSheet.create({
  container: {
    flex: 1,
    width: width,
  },
  containerII: {
    flex: 1,
    width: '80%',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    alignSelf: 'flex-end',
  },
  rowHolder: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    marginTop: 8,
    width: '100%',
  },
  profileImage: {},
  profileImageHolder: {
    width: 48,
    height: 48,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  commentsHolder: {
    opacity: 0.9,
    padding: 8,
    backgroundColor: '#d7d7d7',
    justifyContent: 'center',
    alignItems: 'flex-end',
    borderRadius: 8,
    width: '100%',
  },
  Text: {
    ...iranSans,
    maxWidth: 280,
    marginHorizontal: 8,
  },
  animatedView: {
    width: width,
    position: 'absolute',
    bottom: 0,
    ...centerAll,
    backgroundColor: 'white',
    marginBottom: 16,
  },
  textInput: {
    backgroundColor: '#d7d7d7',
    maxWidth: width - 50,
    minWidth: width - 100,
    marginTop: 16,
    borderRadius: 16,
    textAlign: 'center',
    ...iranSans,
  },
  selectIcon: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 4,
    backgroundColor: MAIN_COLOR,
    width: width,
  },
  button: {
    marginTop: 16,
    width: width / 2,
    paddingVertical: 16,
    backgroundColor: 'green',
    ...centerAll,
    borderRadius: 16,
  },
});
export default Styles;
