import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  FlatList,
  Animated,
  TextInput,
  Dimensions,
  Keyboard,
} from 'react-native';
const {width, height} = Dimensions.get('screen');
import {useTheme, Portal, FAB} from 'react-native-paper';
import {useSafeArea} from 'react-native-safe-area-context';
import {AirbnbRating, Rating} from 'react-native-ratings';
import Icon from 'react-native-vector-icons/FontAwesome5';

import styles from './styles';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {
  fMainColor,
  fWhite,
  iranSans,
  Tac,
  h6,
  fRow,
  centerAll,
  center,
  spaceB,
  Flex,
} from '../../../../values/Theme';
import {
  SUBMIT_TITLE,
  COMMENT_TITLE,
  NO_DATA,
  SEND_COMMENT_TITLE,
} from '../../../../values/Strings';
import {MAIN_COLOR} from '../../../../values/Colors';
import CustomDialog from '../../../../components/Modal';
const Comments = ({navigation}) => {
  const data = navigation.state.params.data;

  const safeArea = useSafeArea();

  let refInput = null;
  const [comment, setComment] = useState(false);
  const [submit, setSubmit] = useState(false);
  const [visibility, setVisibliti] = useState(true);
  const [text, setText] = useState('');
  const scrollY = new Animated.Value(height / 2);
  const [modal, setModal] = useState(false);
  useEffect(() => {
    if (comment) {
      starAnimation();
    } else {
      scrollY.setValue(height / 2);
    }
  }, [comment]);

  const starAnimation = () => {
    Animated.timing(scrollY, {
      useNativeDriver: true,
      duration: 500,
      toValue: 0,
    }).start();
  };
  const transformStyle = {
    transform: [
      {
        translateY: scrollY,
      },
    ],
  };

  const renderItem = ({item}) => {
    return (
      <View style={styles.containerII}>
        <View style={styles.rowHolder}>
          <View style={styles.commentsHolder}>
            <View style={[fRow, center, spaceB, {width: '100%'}]}>
              <Rating
                tintColor="#d7d7d7"
                starContainerStyle={{backgroundColor: 'red'}}
                reviewColor="white"
                selectedColor="red"
                startingValue={item.rate}
                type="star"
                ratingColor="green"
                imageSize={15}
                ratingBackgroundColor="red"
                ratingCount={5}
                defaultRating={1}
                readonly={true}
                // style={{paddingVertical: 10, backgroundColor: 'transparent'}}
              />
              <Text style={[styles.Text, h6]}>{item.date}</Text>
            </View>
            <Text style={styles.Text}>
              {item.userId.name || 'ناشناس'}: {'\n'}
              {item.content}
            </Text>
          </View>
          <View style={styles.profileImageHolder}>
            <Icon style={styles.profileImage} name="user" size={25} />
          </View>
        </View>
      </View>
    );
  };
  const onCommentPress = () => {
    setComment(!comment);
  };
  const onChangeText = (text) => {
    setText(text);
  };
  const onSubmit = () => {
    if (text.length > 2) {
      setModal(true);
      setText('');
    }
    Keyboard.dismiss();
    setComment(false);
    refInput.clear();
  };
  const onAcceptBtn = () => {
    setModal(false);
    //send comment to server
  };
  const onScroll = (e) => {
    if (e.nativeEvent.contentOffset.y > 10) {
      setVisibliti(false);
    } else {
      setVisibliti(true);
    }
  };
  if (!data) {
    return (
      <View style={[Flex, centerAll]}>
        <Text>{NO_DATA}</Text>
      </View>
    );
  }
  return (
    <View style={styles.container}>
      <FlatList
        style={{marginBottom: 50}}
        data={data}
        renderItem={renderItem}
        keyExtractor={(item, index) => item._id}
        onScroll={onScroll}
      />

      <Animated.View style={[styles.animatedView, transformStyle]}>
        <TouchableOpacity
          onPress={onCommentPress}
          activeOpacity={0.6}
          style={styles.selectIcon}>
          <Icon name="chevron-down" size={25} color="white" />
        </TouchableOpacity>
        <TextInput
          placeholder={COMMENT_TITLE}
          ref={(input) => {
            refInput = input;
          }}
          onChangeText={onChangeText}
          multiline
          style={styles.textInput}
          value={text}
        />
        <AirbnbRating
          size={20}
          showRating={false}
          starStyle={{marginTop: 10}}
        />
        <TouchableOpacity onPress={onSubmit} style={styles.button}>
          <Text style={[fWhite, iranSans]}>{SUBMIT_TITLE}</Text>
        </TouchableOpacity>
      </Animated.View>
      <Portal>
        <FAB
          visible={!comment && visibility}
          icon={'message'}
          style={{
            position: 'absolute',
            bottom: safeArea.bottom + 65,
            right: 16,
          }}
          color="white"
          theme={{
            colors: {
              accent: MAIN_COLOR,
            },
          }}
          onPress={() => {
            setComment(true);
          }}
        />
      </Portal>
      <CustomDialog
        title={SEND_COMMENT_TITLE}
        state={modal}
        oncancel={() => setModal(false)}
        accept={onAcceptBtn}
      />
    </View>
  );
};
export default Comments;
