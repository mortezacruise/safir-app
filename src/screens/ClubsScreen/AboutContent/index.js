import {Accordion} from 'native-base';
import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Styles from './styles';
import {
  fRow,
  bgWhite,
  iranSans,
  mV8,
  mH8,
  h4,
  el1,
  m8,
  pad8,
  r8,
  center,
  spaceB,
  padH4,
  padH8,
  h5,
  mV4,
} from '../../../values/Theme';
import {Rating} from 'react-native-ratings';
import {navigate} from '../../../../navigationRef';
import {MY_BALANCE, YOUR_MENTIONS, TOMAN} from '../../../values/Strings';
const AboutContent = (props) => {
  let IconDown = <Icon style={Styles.Icon} name="chevron-down" />;
  let IconLeft = <Icon style={Styles.Icon} size={23} name="chevron-left" />;

  const onCommentPress = () => {
    navigate('CommentScreen', {data: props.data.comments});
  };
  return (
    <View style={Styles.Container}>
      <View style={[fRow, bgWhite, Styles.Content]}>
        <Rating
          starContainerStyle={{backgroundColor: 'red'}}
          reviewColor="white"
          selectedColor="red"
          startingValue={2.5}
          type="star"
          ratingColor="red"
          imageSize={20}
          ratingBackgroundColor="red"
          ratingCount={5}
          defaultRating={1}
          readonly={true}
          style={{paddingVertical: 10, backgroundColor: 'white'}}
        />
        <Text style={[iranSans, mH8, h5]}>امتیاز</Text>
      </View>
      <TouchableOpacity
        onPress={onCommentPress}
        style={[fRow, bgWhite, el1, padH8, r8, center, spaceB, mV8]}>
        <Icon name="chevron-left" size={20} solid />
        <Text style={[iranSans, h5, mV8]}> {YOUR_MENTIONS}</Text>
      </TouchableOpacity>
      <View
        // onPress={onCommentPress}
        style={[fRow, bgWhite, el1, padH8, r8, center, spaceB, mV4]}>
        <Text style={[iranSans]}> {props?.bal || 0} ریال</Text>
        <Text style={[iranSans, h5, mV8]}> {MY_BALANCE}</Text>
      </View>
    </View>
  );
};
export default AboutContent;
{
}
