import axios from "axios";
import React, { useEffect, useState } from "react";
import {
  BackHandler,
  ImageBackground,
  ScrollView,
  Text,
  View,
  Dimensions,
  Image
} from "react-native";
import { Indicator } from "../../../Components/Indicator";
import { NEW_PRICE, TOMAN, OLD_PRICE, PHONES, ADDRESS } from "../../../Values/Strings";
import {
  bgAccentColor,
  bgWhite,
  centerAll,
  el1,
  fBlack,
  fGreen,
  Flex,
  fLineT,
  fRed,
  fRow,
  fWhite,
  h2,
  h3,
  h4,
  h5,
  iranSans,
  m8,
  mH16,
  mV8,
  pad8,
  padH8,
  padV8,
  posAbs,
  r8,
  s48,
  spaceB,
  bgGray,
  s32,
  center,
  s24,
  mV4
} from "../../../Values/Theme";
// import KamponFooter from "./KamponFooter";
import KamponTab from "./KamponTab";
import { TouchableOpacity } from "react-native-gesture-handler";
import BackgroundCursor from "../../../Components/BackgroundCursor/BackgroundCursor";
import styles from "./styles";
const DiscountsScreen = ({ navigation }) => {
  const [discount, setDiscount] = useState([]);
  const [isLoading, setLoading] = useState(true);

  const handleBackPress = () => {
    navigation.pop();
    return true;
  };
  let newPrice = "";
  let realPrice = "";

  useEffect(() => {
    BackHandler.addEventListener("hardwareBackPress", handleBackPress);

    return () => {
      BackHandler.removeEventListener("hardwareBackPress", handleBackPress);
    };
  }, [navigation]);
  //=============================== DISCOUNTS DATA SEPREATE ========================

  if (isLoading) return <Indicator />;

  return (
    <View style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false} style={{ marginBottom: 45 }}>
        {/* <BackgroundCursor /> */}
        <View style={styles.topContentHolder}>
          <Text style={[styles.topContentText, mH16]}>{discount.disTitle}</Text>
          <Text style={styles.topContentTextOwner}>{discount.ownerId.oName}</Text>
          <Text style={[iranSans, h4, mV8, mH16]}> {discount.disDescription}</Text>
          <ImageBackground
            style={styles.imageBacground}
            source={require("../../Assets/Png/discount.png")}
          >
            <Text style={[iranSans, h3, fWhite]}>%{discount.disPercent}</Text>
          </ImageBackground>
        </View>
        <View style={[bgWhite, el1, m8, padV8, padH8, r8]}>
          <View style={[fRow, spaceB]}>
            <Text style={[iranSans, h4, mV8, fGreen]}>{NEW_PRICE}</Text>
            <Text style={[iranSans, h4, mV8, fGreen]}>
              {newPrice} {TOMAN}
            </Text>
          </View>
          <View style={styles.spaeacer} />
          <View style={[fRow, spaceB]}>
            <Text style={[iranSans, h5, mV8, fRed]}>{[OLD_PRICE]}</Text>
            <Text style={[iranSans, h5, mV8, fRed, fLineT]}>
              {realPrice} {TOMAN}
            </Text>
          </View>
          <View style={styles.spaeacer} />
          <View style={[fRow, spaceB]}>
            <Text style={[iranSans, h5, mV8, fBlack]}>{PHONES}</Text>
            <Text style={[iranSans, h5, mV8, fBlack]}>{discount.ownerId.oPhoneNumber}</Text>
          </View>
          <View style={styles.spaeacer} />
          <View style={[fRow]}>
            <Text style={[iranSans, h5, mV8, fBlack]}> {ADDRESS} </Text>
            <Text style={styles.ownerAddresText}>{discount.ownerId.oAddress}</Text>
          </View>
        </View>
        <TouchableOpacity
          style={[fRow, bgWhite, el1, m8, pad8, r8, center, spaceB]}
          onPress={onNavigateToReport}
        >
          <Text style={[iranSans, h5, mV8]}>گزارش اشکال</Text>
          <Image style={[s24]} source={require("../../Assets/Png/back.png")} />
        </TouchableOpacity>
        <KamponTab tabContent={discount} />
      </ScrollView>
      {/* <KamponFooter id={discount._id} navigate={navigation.navigate} /> */}
    </View>
  );
};

export default DiscountsScreen;
