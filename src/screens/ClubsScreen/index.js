import React, {useEffect, useState, useContext} from 'react';
import {View, Text, ScrollView, TouchableOpacity} from 'react-native';
import Styles from './style';
import MainContent from './MainContent';
import AboutContent from './AboutContent';
import {Indicator} from '../../components/Indicator';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Icon1 from 'react-native-vector-icons/EvilIcons';
import {
  fRow,
  bgWhite,
  el1,
  m8,
  pad8,
  r8,
  center,
  spaceB,
  iranSans,
  h5,
  mV8,
  h4,
  h3,
  padV8,
  h6,
  padH8,
} from '../../values/Theme';
import IntroSlider from '../../components/IntroSlider';
import LowerContent from './LowerContent';
import {toggleBookmark} from '../../api/post/toggleBookmark';
import {share} from '../../HelperFuncs/share';
import {Context as User} from '../../context/userDataContext';
import {navigate} from '../../../navigationRef';
import {getUserBalance} from '../../api/get/getUserBalance';
const ClubsScreen = ({navigation}) => {
  const userContext = useContext(User);
  const {getMemberInformation, state} = userContext;

  const [loading, setLoading] = useState(true);
  const [disCountsData, setDisCountsData] = useState(null);
  const [clubsData, setClubsData] = useState(null);
  const [isInBookmarked, setIsInBookmarked] = useState(false);
  const [loadingBookmark, setLoadingBookmark] = useState(false);
  const [userBal, setUderBal] = useState(null);

  //==========================  CLUBS_DATA   ========================
  let clubsSliderData = [];
  let clubsContentData = [];
  let clubsAbuotData = [];
  //==========================  CLUBS_DATA   ========================

  //================================================================
  useEffect(() => {
    const clubParam = navigation.getParam('clubParams');
    clubParam && setClubsData(clubParam);
  }, []);
  useEffect(() => {
    getUserBallancee();
    clubsData && setLoading(false);
    clubsData && chekooIsBook();
  }, [clubsData]);

  const getUserBallancee = async () => {
    if (clubsData) {
      const res = await getUserBalance(clubsData._id);
      res.data && setUderBal(res.data.etebar);
    }
  };

  const chekooIsBook = () => {
    state.userInfo.storeBookmarks &&
      state.userInfo.storeBookmarks.map((item) => {
        item._id === clubsData._id && setIsInBookmarked(true);
      });
  };
  //==========================  PREAPERING_CLUBS_DATA ==============================
  if (clubsData) {
    const {comments, aboutClub} = clubsData;
    clubsSliderData = clubsData.slides;
    clubsContentData = clubsData;
    clubsAbuotData = {comments, aboutClub};
  }
  //==========================  PREAPERING_CLUBS_DATA ==============================

  if (loading) {
    return <Indicator />;
  }
  if (disCountsData) {
    return (
      <View>
        <Text>wrong</Text>
      </View>
    );
  }
  const onBookPress = async () => {
    setIsInBookmarked(!isInBookmarked);
    const resBook = await toggleBookmark(clubsData._id);
    if (resBook === 2017) {
      getMemberInformation();
    }

    if (resBook === 2018) {
      getMemberInformation();
    }
  };
  const onSharePress = async () => {
    const resShare = await share();
  };
  return (
    <View style={Styles.Container}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        renderToHardwareTextureAndroid
        scrollEventThrottle={1}
        alwaysBounceVertical>
        <IntroSlider slides={clubsSliderData} />
        <View style={{padding: 8}}>
          <MainContent data={clubsContentData} />
          <TouchableOpacity
            onPress={() => navigate('ReportUser')}
            style={[fRow, bgWhite, el1, m8, padH8, r8, center, spaceB]}>
            <Icon name="chevron-left" size={18} />
            <Text style={[iranSans, h5, mV8]}>گزارش اشکال</Text>
          </TouchableOpacity>
          <AboutContent
            title={clubsContentData.title}
            data={clubsAbuotData}
            bal={userBal}
          />
          <LowerContent clubsData={clubsData} />
        </View>
      </ScrollView>
      <View style={Styles.Footer}>
        <Icon
          onPress={onBookPress}
          name={'bookmark'}
          color={'white'}
          size={23}
          solid={isInBookmarked}
        />

        <Icon1
          name="share-google"
          size={35}
          color="white"
          onPress={onSharePress}
        />
      </View>
    </View>
  );
};
export default ClubsScreen;
