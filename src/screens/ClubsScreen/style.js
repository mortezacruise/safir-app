import {StyleSheet} from 'react-native';
import {
  Flex,
  bgMainColor,
  accentColor,
  iranSans,
  h4,
  fWhite,
} from '../../values/Theme';
import {MAIN_COLOR} from '../../values/Colors';
const Styles = StyleSheet.create({
  Container: {
    ...Flex,
    backgroundColor: accentColor,
    // padding: 8,
  },
  Footer: {
    width: '100%',
    height: 45,
    backgroundColor: MAIN_COLOR,
    alignItems: 'center',
    justifyContent: 'space-around',
    flexDirection: 'row',
    paddingHorizontal: 32,
  },
  Text: {
    ...iranSans,
    ...h4,
    ...fWhite,
  },
});
export default Styles;
