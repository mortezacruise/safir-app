import React, {useEffect, useState} from 'react';
import {View, Text, TouchableOpacity, ScrollView} from 'react-native';
import {
  padV8,
  pad8,
  iranSans,
  mH8,
  borderLEFT,
  borderRight,
  Flex,
} from '../../../values/Theme';
import ViewPager from '@react-native-community/viewpager';
import {DARK_GREEN, MAIN_COLOR} from '../../../values/Colors';
import styles from './styles';
import {screenHeight} from '../../../values/Constants';
const array = [
  {name: 'شرایط خرید', page: 0},
  {name: 'ویژگی ها', page: 1},
];
const LowerContent = ({clubsData}) => {
  const [position, setposition] = useState(0);
  const [page, setPage] = useState(1);
  const onItemPressed = (itemPage) => {
    setPage(itemPage);
  };
  const viewPager = () => {
    return array.map((item, index) => {
      return (
        <TouchableOpacity
          key={index}
          style={[
            styles.lowerContentSelect,
            {
              backgroundColor:
                page === item.page ? 'rgba(169,0,103,0.7)' : 'white',
            },
            ,
            ,
            index === 0 ? borderLEFT : borderRight,
          ]}
          onPress={() => onItemPressed(item.page)}>
          <Text
            style={[
              styles.Text,
              padV8,
              {color: page === item.page ? 'white' : 'black'},
            ]}>
            {item.name}
          </Text>
        </TouchableOpacity>
      );
    });
  };
  return (
    <View
      style={[
        pad8,
        Flex,
        {
          minHeight: screenHeight / 4,
        },
      ]}>
      <View style={styles.lowerContent}>{viewPager()}</View>
      {page === 0 ? (
        <ScrollView
          showsVerticalScrollIndicator={false}
          key="1"
          style={{height: '100%'}}>
          {clubsData.conditions.length > 0 &&
            clubsData.conditions.map((item, index) => (
              <Text key={index} style={[iranSans, mH8]}>
                {item}
              </Text>
            ))}
        </ScrollView>
      ) : (
        <ScrollView
          showsVerticalScrollIndicator={false}
          key="2"
          style={{height: '100%'}}>
          {clubsData.features.length > 0 &&
            clubsData.features.map((item, index) => (
              <Text key={index} style={[iranSans, mH8]}>
                {item}
              </Text>
            ))}
        </ScrollView>
      )}
    </View>
  );
};
export default LowerContent;
{
  /* <ViewPager
transitionStyle="curl"
style={styles.viewPager}
initialPage={1}
ref={(ref) => setPage(ref)}
// showPageIndicator={true}
onPageSelected={(i) => {
  setposition(i.nativeEvent.position);
}}> */
}
