import {Dimensions, StyleSheet} from 'react-native';
import {bottomRadiuse, centerAll, iranSans} from '../../../values/Theme';
import {MAIN_COLOR} from '../../../values/Colors';
// import {"red"} from '../../../values/Colors';
const {width, height} = Dimensions.get('window');
export default styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  mainContent: {
    ...centerAll,
    padding: 8,
  },
  topImage: {
    width: width - 10,
    height: height / 3.3,
    borderRadius: 16,
  },
  rowContent: {
    flexDirection: 'row-reverse',
    alignItems: 'center',
    width: width,
    height: height / 6,
    padding: 8,
  },
  sellerImage: {
    borderRadius: 100,
    width: width / 5,
    height: width / 5,
  },
  contentTextHolder: {
    marginRight: 16,
  },
  Text: {
    ...iranSans,
  },
  horizontalSpacer: {
    height: '100%',
    width: 1.2,
    backgroundColor: 'red',
  },
  price_buy_Holder: {
    borderWidth: 1,
    width: '100%',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  buy: {
    width: '50%',
    backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'center',
  },
  priceHolder: {
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  lowerContentSelect: {
    width: '50%',
    ...centerAll,
  },
  lowerContent: {
    flexDirection: 'row',
    width: '100%',
  },
  viewPager: {
    width: '100%',
    flex: 1,
    // minHeight: height / 4,
    // maxHeight: height,
    // height: '100%',
    // flexBasis: 1,
    flexWrap: 'wrap',
    backgroundColor: '#eee',
    overflow: 'hidden',
  },
});
