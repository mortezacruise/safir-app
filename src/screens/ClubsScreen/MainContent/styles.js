import {StyleSheet} from 'react-native';
import {
  centerAll,
  Flex,
  fRow,
  h3,
  h5,
  iranSans,
  pad16,
  r100,
  fWhite,
  s48,
} from '../../../values/Theme';
const Styles = StyleSheet.create({
  Container: {
    ...Flex,
    marginTop: 8,
  },
  Content: {
    ...fRow,
    justifyContent: 'space-between',
    backgroundColor: 'white',
    borderBottomEndRadius: 8,
    borderBottomStartRadius: 8,
    borderTopEndRadius: 8,
    borderTopStartRadius: 8,
    elevation: 1,
  },
  ContentRight: {
    marginTop: 8,

    ...pad16,
  },
  ContentLeft: {
    textAlign: 'right',
    ...pad16,
  },
  Title: {
    ...h3,
    ...iranSans,
  },
  PercentHolder: {
    ...centerAll,
    ...s48,
  },
  Icons: {
    flex: 1,
    ...fRow,
    justifyContent: 'space-between',

    width: '100%',
  },
  IconsTitle: {
    ...iranSans,
    ...h5,
    textAlign: 'center',
  },
  IconsViewRight: {
    backgroundColor: 'white',
    ...centerAll,
    borderBottomEndRadius: 8,
    borderTopEndRadius: 8,
    width: '32%',
    padding: 16,
    marginVertical: 8,
    elevation: 1,
  },
  IconsViewLeft: {
    backgroundColor: 'white',
    ...centerAll,

    width: '32%',
    padding: 16,
    marginVertical: 8,
    borderBottomStartRadius: 8,
    borderTopStartRadius: 8,
    elevation: 1,
  },
  IconsViewMiddle: {
    backgroundColor: 'white',
    ...centerAll,

    width: '32%',
    padding: 16,
    marginVertical: 8,
    elevation: 1,
  },
  Details: {
    backgroundColor: 'white',
    width: '100%',
    marginBottom: 8,
    borderRadius: 8,
    padding: 4,
    marginTop: 8,
    elevation: 1,
  },
  Address: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    padding: 8,
  },
  Text: {
    ...iranSans,
  },
  MapView: {
    backgroundColor: '#a8a8a8',
    width: '100%',
    marginBottom: 8,
    borderRadius: 8,
    height: 100,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
  },
  MApTitle: {
    ...h3,
    ...iranSans,
    position: 'absolute',
    textAlign: 'center',
    ...fWhite,
  },
  mapIAmge: {
    width: '100%',
    height: '100%',
  },
  discountPercentBg: {
    position: 'absolute',
    width: 48,
    height: 48,
    backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'center',
    display: 'flex',
    right: 0,
  },

  discountPercent: {
    color: 'white',
    fontSize: 15,
  },
});
export default Styles;
