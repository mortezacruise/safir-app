import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  PickerIOSComponent,
  ImageBackground,
} from 'react-native';
import {Rating} from 'react-native-ratings';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {fWhite, h5, iranSans, h6, h3, h4} from '../../../values/Theme';
import Styles from './styles';
import MApImage from '../../../assets/Png/GoogleMapTA.png';
import {MAIN_COLOR} from '../../../values/Colors';
import {navigate} from '../../../../navigationRef';
import Spacer from '../../../components/Spacer';
const CARD_TITLE = 'عضویت با کارت';
const MOBILE_TITLE = 'عضویت با اپلیکیشن';
const PEOPLE_TITLE = '۴۵۰۰ عضو';
const ADDRESS_TITLE = 'آدرس';
const PHONE_NUMBER = 'تلفن';
const SHOW_IN_MAP = 'نمایش روی نقشه';
const INSTALLANCE = 'اقساط';
const ESCOUNT = 'کارمزد';
const MainContent = (props) => {
  let Application = null;
  let card = null;
  let i;
  const {location, installments, commission, slides} = props.data;

  const {
    coordinate,
    phone,
    isAvtive,
    balance,
    thumbnail,
    title,
    subtitle,
    address,
    phoneNumber,
    district,
    rating,
  } = props.data.owner;

  const onMapPress = () => {
    navigate('Map', {location: [location.lng, location.lat], image: slides[0]});
  };

  return (
    <View style={Styles.Container}>
      <View style={Styles.Content}>
        <View style={Styles.ContentRight}></View>
        <View style={Styles.ContentLeft}>
          <Text style={[iranSans, h3]}>{props.data.title}</Text>
          <Text style={[iranSans, h5]}>{title}</Text>
          <Text style={[Styles.Title, h5, {opacity: 0.5}]}>
            {props.data?.category?.titleFa}
          </Text>
        </View>
      </View>

      <View style={Styles.Details}>
        <View style={Styles.Address}>
          <Text style={[Styles.Text, {marginRight: 4}]}>{installments}</Text>
          <Text style={Styles.Text}>{INSTALLANCE}:</Text>
        </View>
        <Spacer />
        <View style={Styles.Address}>
          <Text style={[Styles.Text, {marginRight: 4}]}>{commission}</Text>
          <Text style={Styles.Text}>{ESCOUNT}:</Text>
        </View>
        <Spacer />

        <View style={Styles.Address}>
          {phone &&
            phone.map((phone, index) => (
              <Text key={index} style={[Styles.Text, {marginRight: 4}]}>
                {phone}
              </Text>
            ))}
          <Text style={Styles.Text}>{PHONE_NUMBER}:</Text>
        </View>
        <Spacer />

        <View style={Styles.Address}>
          <Text style={[Styles.Text, {marginRight: 4}]}>{address}</Text>
          <Text style={Styles.Text}>{ADDRESS_TITLE}:</Text>
        </View>
      </View>
      <TouchableOpacity
        activeOpacity={0.6}
        style={Styles.MapView}
        onPress={onMapPress}>
        <Image source={MApImage} style={Styles.mapIAmge} />
        <Text style={Styles.MApTitle}>{SHOW_IN_MAP}</Text>
      </TouchableOpacity>
    </View>
  );
};
export default MainContent;

{
  //   <Rating
  // starContainerStyle={{backgroundColor: 'red'}}
  // reviewColor="white"
  // selectedColor="red"
  // startingValue={rating}
  // type="star"
  // ratingColor="red"
  // imageSize={20}
  // ratingBackgroundColor="red"
  // ratingCount={5}
  // defaultRating={1}
  // readonly={true}
  // style={{paddingVertical: 10, backgroundColor: 'white'}}
  // />
}
{
  /* <ImageBackground
style={Styles.PercentHolder}
source={require('../../../assets/Png/discount.png')}>
<Text style={[fWhite, iranSans, h5]}>{percent}%</Text>
</ImageBackground> */
}
