import React, {useContext, useEffect, useMemo, memo} from 'react';
import {
  Image,
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableWithoutFeedback,
  ImageBackground,
} from 'react-native';
import {centerAll} from '../../../values/Theme';
import DisAndClubsContainer from '../../../baseComponent/DisAndClubsContainer';
import {
  MOST_VIEW,
  MOST_NEW,
  TODAY_OFFER,
  MOST_DISCOUNT_HOME,
  MOST_INSTALLMENT,
  POPULAR,
} from '../../../values/Strings';
import FastImage from 'react-native-fast-image';
import {navigate} from '../../../../navigationRef';
const {height, width} = Dimensions.get('screen');
const BannerHeight = height / 5;
const DiscountListContainer = ({context}) => {
  const {homeScreenDataState, typeFilter, cityFilter} = context.state;

  const MostView = homeScreenDataState?.mostView;
  const firstBanner = homeScreenDataState?.banner;
  const Newest = homeScreenDataState?.newest;
  const SecondBanner = homeScreenDataState?.secondBanner;
  const TodayOffer = homeScreenDataState?.todayOffer;
  const ThirdBanner = homeScreenDataState?.thirdBanner;
  const MostInstallment = homeScreenDataState?.mostInstallment;

  const mostViewItems = () => {
    return (
      <DisAndClubsContainer
        filter={typeFilter}
        data={MostView}
        title={MOST_VIEW}
        urlBody="mostView"
      />
    );
  };
  const theNewestItem = () => {
    return (
      <DisAndClubsContainer
        filter={typeFilter}
        data={Newest}
        title={MOST_NEW}
        urlBody="newest"
      />
    );
  };
  const todatsOfferItems = () => {
    return (
      <DisAndClubsContainer
        filter={typeFilter}
        data={TodayOffer}
        title={TODAY_OFFER}
        urlBody="todayOffer"
      />
    );
  };
  const onBannerPress = (type) => {
    switch (type) {
      case 1:
        firstBanner.store &&
          navigate('ClubsScreen', {clubParams: firstBanner.store});
        break;
      case 2:
        if (SecondBanner.store) {
          navigate('ClubsScreen', {clubParams: SecondBanner.store});
        }
        break;
      case 3:
        if (ThirdBanner.store) {
          navigate('ClubsScreen', {clubParams: ThirdBanner.store});
        }
        break;
      default:
        break;
    }
  };
  const mostInstallment = () => {
    return (
      <DisAndClubsContainer
        filter={typeFilter}
        data={MostInstallment}
        most={true}
        title={MOST_INSTALLMENT}
        urlBody="mostInstallment"
      />
    );
  };
  const firstBannerImage = () => {
    if (firstBanner) {
      return (
        <TouchableWithoutFeedback
          style={{position: 'relative', width: '100%', height: BannerHeight}}
          onPress={() => onBannerPress(1)}>
          <FastImage
            style={{width: '100%', height: BannerHeight, marginVertical: 16}}
            source={{uri: firstBanner.image}}
          />
        </TouchableWithoutFeedback>
      );
    } else {
      return null;
    }
  };
  const secoundBannerImage = () => {
    if (SecondBanner) {
      return (
        <TouchableWithoutFeedback onPress={() => onBannerPress(2)}>
          <FastImage
            style={{width: '100%', height: BannerHeight, marginVertical: 16}}
            source={{uri: SecondBanner.image}}
          />
        </TouchableWithoutFeedback>
      );
    } else {
      return null;
    }
  };
  const thirdBannerImage = () => {
    if (ThirdBanner) {
      return (
        <TouchableWithoutFeedback onPress={() => onBannerPress(3)}>
          <FastImage
            style={{width: '100%', height: BannerHeight, marginVertical: 16}}
            source={{uri: ThirdBanner.image}}
          />
        </TouchableWithoutFeedback>
      );
    } else {
      return null;
    }
  };
  return (
    <View style={Styles.container}>
      {mostViewItems()}
      {firstBannerImage()}
      {theNewestItem()}
      {secoundBannerImage()}
      {todatsOfferItems()}
      {thirdBannerImage()}
      {mostInstallment()}
    </View>
  );
};
export default React.memo(DiscountListContainer);

const Styles = StyleSheet.create({
  container: {flex: 1, ...centerAll, marginTop: 18},
});
