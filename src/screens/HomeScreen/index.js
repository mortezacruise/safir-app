import React, {useContext, useEffect, useState} from 'react';
import {
  ScrollView,
  StatusBar,
  View,
  Text,
  TouchableOpacity,
  Linking,
} from 'react-native';
import {Context as DataContext} from '../../context/DataContext';
import {Context as User} from '../../context/userDataContext';
import {MAIN_COLOR} from '../../values/Colors';
import DiscountListContainer from './DiscountListContainer';
import CategoryList from './CategoryList';
import styles from './styles';
import TopContent from './TopContent';
import Version from 'react-native-version-number';
import {fRow, center} from '../../values/Theme';
import {UPDATE_TITLE, UPDATE} from '../../values/Strings';
import Icon from 'react-native-vector-icons/AntDesign';

import PushPole from 'pushpole-react-native';
const HomeScreen = () => {
  const userContext = useContext(User);
  const {getMemberInformation} = userContext;
  const context = useContext(DataContext);
  // const {fetchAlldiscounts} = context;
  // console.log({STATE: context.state});
  const [updateVisible, setUpdateVisible] = useState(true);

  useEffect(() => {
    PushPole.getId((pushpoleId) => {});
    // PushPole.addEventListener(PushPole.EVENTS.RECEIVED, (notification) => {
    // });
    getMemberInformation();
    context.getAllStore();
  }, []);
  const onUpdatePress = () => {
    Linking.openURL(context.state?.appVersion?.link)
      .then((res) => {
        console.log({res});
      })
      .catch((e) => {
        console.log({e});
      });

    console.log('update');
  };
  const updateNotif = () => {
    const version = Version.appVersion;
    if (context.state?.appVersion?.version !== version && updateVisible) {
      return (
        <View style={styles.updateContent}>
          <Icon
            name="close"
            size={17}
            color={'white'}
            onPress={() => setUpdateVisible(false)}
          />
          <TouchableOpacity onPress={onUpdatePress}>
            <Text
              style={[
                styles.text,
                {textDecorationLine: 'underline', fontSize: 18},
              ]}>
              {UPDATE}
            </Text>
          </TouchableOpacity>
          <Text style={styles.text}>{UPDATE_TITLE}</Text>
        </View>
      );
    } else {
      return null;
    }
  };
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor={MAIN_COLOR} barStyle="light-content" />
      <ScrollView showsVerticalScrollIndicator={false} scrollEventThrottle={1}>
        {updateNotif()}
        <TopContent />
        <CategoryList />
        <DiscountListContainer context={context} />
      </ScrollView>
    </View>
  );
};

export default HomeScreen;
