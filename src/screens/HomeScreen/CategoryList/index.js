import React, {useContext, useMemo} from 'react';
import {FlatList, View} from 'react-native';
import CategoryRow from '../../../baseComponent/CategoryRow';
import {Context as DataContext} from '../../../context/DataContext';
import styles from './styles';
import {navigate} from '../../../../navigationRef';

const CategoryList = () => {
  //======================== CONTEXT=================
  const context = useContext(DataContext);
  const {state, fetchCategoriesData} = context;
  const data = state?.categoiresState;

  //======================== CONTEXT=================

  const onPress = (id) => {
    navigate('CategoriesScreen', {cat: id});
  };

  const renderItem = ({item}) => {
    return <CategoryRow onCategoryPress={onPress} cat={item} />;
  };

  return useMemo(() => {
    return (
      <View style={styles.container}>
        <FlatList
          inverted
          showsHorizontalScrollIndicator={false}
          horizontal
          data={data}
          renderItem={renderItem}
          keyExtractor={(item, index) =>
            Math.random(Math.pow(index) * 3.5).toString()
          }
        />
      </View>
    );
  }, [data]);
};
export default CategoryList;
