import {StyleSheet, Dimensions} from 'react-native';
import {centerAll} from '../../../values/Theme';

export default styles = StyleSheet.create({
  container: {
    flex: 1,
    ...centerAll,
  },
});
