import React, {useEffect, useState, useContext} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
const TOP_TITLE = 'دکتر تخفیف';
import Styles from './styles';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {LIGHT_GRAY} from '../../../values/Colors';
import {ActionSheet} from 'native-base';
import {
  RASHT,
  LAHIJAN,
  ASTANE,
  REMOVE_FILETR,
  FILTER_ACCORDING_CITY,
  CITY,
  DIS_FILTER,
  CLUB_FILTER,
  FILTER_ACCORDING_TYPE,
  TYPE,
} from '../../../values/Strings';
import {Context as DataContext} from '../../../context/DataContext';
import {Tac, iranSans, mH8} from '../../../values/Theme';
import {navigate} from '../../../../navigationRef';
var DESTRUCTIVE_INDEX = 3;
var CANCEL_INDEX = 4;
const SEARCH_TITLE = 'جستجو در دکتر کلابز';
const TopContentHome = ({onSearchPress}) => {
  //============================== CONTEXT ==============================
  const context = useContext(DataContext);
  const {setCityFilter, setTypeFilter} = context;

  //============================== CONTEXT ==============================
  const [state, setState] = useState({first: '', middle: '', end: ''});
  useEffect(() => {
    setCityFilter(state.end);
  }, [state.end]);
  useEffect(() => {
    setTypeFilter(state.middle);
  }, [state.middle]);
  const onFirstPressed = () => {
    const BUTTONS = [
      'morteza',
      'morteza 1',
      'morteza 2',
      'morteza 3',
      'morteza 4',
    ];
    ActionSheet.show(
      {
        options: BUTTONS,
        cancelButtonIndex: CANCEL_INDEX,
        destructiveButtonIndex: DESTRUCTIVE_INDEX,
        title: 'Testing ActionSheet',
      },
      (buttonIndex) => {
        setState({...state, first: BUTTONS[buttonIndex]});
      },
    );
  };
  const onMiddlePressed = () => {
    const BUTTONS = [DIS_FILTER, CLUB_FILTER, REMOVE_FILETR];
    ActionSheet.show(
      {
        options: BUTTONS,
        cancelButtonIndex: CANCEL_INDEX,
        destructiveButtonIndex: DESTRUCTIVE_INDEX,
        title: FILTER_ACCORDING_TYPE,
      },
      (buttonIndex) => {
        if (BUTTONS[buttonIndex] === REMOVE_FILETR) {
          setState({...state, middle: TYPE});
        } else {
          setState({...state, middle: BUTTONS[buttonIndex]});
        }
      },
    );
  };
  const onEndPressed = () => {
    const BUTTONS = [RASHT, LAHIJAN, ASTANE, REMOVE_FILETR];
    ActionSheet.show(
      {
        options: BUTTONS,
        cancelButtonIndex: CANCEL_INDEX,
        destructiveButtonIndex: DESTRUCTIVE_INDEX,
        title: FILTER_ACCORDING_CITY,
      },
      (buttonIndex) => {
        if (BUTTONS[buttonIndex] === REMOVE_FILETR) {
          setState({...state, end: CITY});
        } else {
          setState({...state, end: BUTTONS[buttonIndex]});
        }
      },
    );
  };
  const onSearchPressed = () => {
    navigate('SearchContainer');
  };

  return (
    <View style={Styles.Container}>
      <Text style={Styles.TopTitle}>{TOP_TITLE}</Text>
      <TouchableWithoutFeedback onPress={onSearchPressed}>
        <View style={Styles.Search}>
          <Text style={[Tac, iranSans, mH8]}>{SEARCH_TITLE}</Text>
          <Icon name="search" color={LIGHT_GRAY} size={20} />
        </View>
      </TouchableWithoutFeedback>
      <View style={Styles.DropDownView}>
        <TouchableOpacity style={Styles.DropDownd} onPress={onFirstPressed}>
          <Icon name="chevron-down" color={LIGHT_GRAY} />
          <Text style={Styles.DropDownText}>
            {state.first ? state.first : 'تخفیف'}
          </Text>

          {/* <Icon name="percentage" color={LIGHT_GRAY} /> */}
        </TouchableOpacity>
        <TouchableOpacity style={Styles.DropDownd} onPress={onMiddlePressed}>
          <Icon name="chevron-down" color={LIGHT_GRAY} />
          <Text style={Styles.DropDownText}>
            {state.middle ? state.middle : TYPE}
          </Text>
          {/* <Icon name="percentage" color={LIGHT_GRAY} /> */}
        </TouchableOpacity>
        <TouchableOpacity style={Styles.DropDownd} onPress={onEndPressed}>
          <Icon name="chevron-down" color={LIGHT_GRAY} />
          <Text style={Styles.DropDownText}>
            {state.end ? state.end : CITY}
          </Text>

          {/* <Icon name="percentage" color={LIGHT_GRAY} /> */}
        </TouchableOpacity>
      </View>
    </View>
  );
};
export default TopContentHome;
