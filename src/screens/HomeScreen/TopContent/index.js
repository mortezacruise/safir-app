import React, {useEffect, useState, useContext} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
const TOP_TITLE = 'دکتر تخفیف';
import Styles from './styles';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {LIGHT_GRAY} from '../../../values/Colors';
import {ActionSheet} from 'native-base';
import {
  RASHT,
  LAHIJAN,
  ASTANE,
  REMOVE_FILETR,
  FILTER_ACCORDING_CITY,
  CITY,
} from '../../../values/Strings';
import {Tac, iranSans, mH8} from '../../../values/Theme';
import {navigate} from '../../../../navigationRef';
var DESTRUCTIVE_INDEX = 3;
var CANCEL_INDEX = 4;
const SEARCH_TITLE = 'جستجو در سفیر  ';
const TopContentHome = ({onSearchPress}) => {
  const [state, setState] = useState({first: '', middle: '', end: ''});

  const onEndPressed = () => {
    const BUTTONS = [RASHT, LAHIJAN, ASTANE, REMOVE_FILETR];
    ActionSheet.show(
      {
        options: BUTTONS,
        cancelButtonIndex: CANCEL_INDEX,
        destructiveButtonIndex: DESTRUCTIVE_INDEX,
        title: FILTER_ACCORDING_CITY,
      },
      (buttonIndex) => {
        if (BUTTONS[buttonIndex] === REMOVE_FILETR) {
          setState({...state, end: CITY});
        } else {
          setState({...state, end: BUTTONS[buttonIndex]});
        }
      },
    );
  };
  const onSearchPressed = () => {
    navigate('Search');
  };

  return (
    <View style={Styles.Container}>
      <View style={Styles.DropDownView}>
        <TouchableOpacity style={Styles.DropDownd} onPress={onEndPressed}>
          <Icon name="chevron-down" color={LIGHT_GRAY} />
          <Text style={Styles.DropDownText}>
            {state.end ? state.end : CITY}
          </Text>
        </TouchableOpacity>
        <TouchableWithoutFeedback onPress={onSearchPressed}>
          <View style={Styles.Search}>
            <Text style={[Tac, iranSans, mH8, {opacity: 0.5}]}>
              {SEARCH_TITLE}
            </Text>
            <Icon name="search" color={LIGHT_GRAY} size={20} />
          </View>
        </TouchableWithoutFeedback>
      </View>
    </View>
  );
};
export default TopContentHome;
