import {StyleSheet, Dimensions} from 'react-native';
import {
  iranSans,
  Tac,
  h2,
  centerAll,
  h5,
  fRow,
  pad8,
  bgWhite,
  r8,
  posAbs,
  h1,
  fWhite,
} from '../../../values/Theme';
import {H2} from 'native-base';
import {LIGHT_GRAY, MAIN_COLOR} from '../../../values/Colors';
const SCREEN_WITH = Dimensions.get('window').width;
const Styles = StyleSheet.create({
  Container: {
    flex: 1,
    ...centerAll,
  },
  TopTitle: {
    ...iranSans,
    ...Tac,
    ...h2,
  },
  Search: {
    marginTop: 18,
    flexDirection: 'row',
    width: '70%',
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'white',
    elevation: 1,
    paddingHorizontal: 8,
    paddingVertical: 12,
    height: 50,
    borderTopEndRadius: 8,
    borderBottomEndRadius: 8,
  },

  DropDownView: {
    justifyContent: 'center',
    alignItems: 'flex-end',
    ...fRow,
    marginTop: 8,
    width: '100%',
  },
  DropDownText: {
    ...iranSans,
    ...h5,
    color: LIGHT_GRAY,
    marginHorizontal: 4,
  },
  EachDropElement: {
    alignItems: 'center',
    justifyContent: 'space-between',
    ...fRow,
    backgroundColor: 'white',
    padding: 12,
    borderRadius: 8,

    width: SCREEN_WITH / 3.8,
  },
  DropDownsHolder: {
    ...fRow,
    width: SCREEN_WITH - 50,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 1,
  },
  DropDownd: {
    flexDirection: 'row',
    width: '25%',
    backgroundColor: 'white',
    elevation: 1,
    padding: 8,
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    borderBottomStartRadius: 8,
    borderTopStartRadius: 8,
    borderEndWidth: 1,
    borderColor: '#d7d7d7',
  },

  ContentDropDown: {
    width: '25%',
    borderRadius: 8,
    backgroundColor: 'transparent',
    borderWidth: 0,
  },
  DropContentText: {
    textAlign: 'right',
    ...iranSans,
    margin: 4,
  },
  RowDropDown: {
    borderRadius: 8,
    backgroundColor: 'white',
    margin: 1,
  },
});
export default Styles;
