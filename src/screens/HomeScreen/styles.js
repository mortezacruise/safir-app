import {StyleSheet} from 'react-native';
import {fRow, iranSans, h5, fWhite, h3} from '../../values/Theme';
import {MAIN_COLOR} from '../../values/Colors';

export default styles = StyleSheet.create({
  updateContent: {
    ...fRow,
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: MAIN_COLOR,
    padding: 4,
  },
  text: {
    ...iranSans,
    ...h3,
    ...fWhite,
  },
});
