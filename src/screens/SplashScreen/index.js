import React, {useEffect, useContext, useState} from 'react';
import {View, Text, Image, BackHandler, Linking} from 'react-native';
import {Context as DataContext} from '../../context/DataContext';
import {checkTokenInDevice} from '../../api/get/checkTokenInDevice';
import styles from './styles';
import VersionNumber from 'react-native-version-number';

import {
  DR_CLUBS_TITLE,
  SAFIR_CARD,
  INSTALLMENT_NETWORK_TITILE,
  FORSE_UPDATE_TITLE,
  EXIT_USER,
} from '../../values/Strings';
import image from '../../assets/Png/Logo.png';
import {h1, fWhite, h4} from '../../values/Theme';
import drLogo from '../../assets/Png/drLogo.png';
import CustomDialog from '../../components/Modal';
const SplashScreen = ({navigation}) => {
  const version = {
    vn: VersionNumber.appVersion,
    bv: VersionNumber.buildVersion,
    pn: VersionNumber.bundleIdentifier,
  };
  const context = useContext(DataContext);
  const {checkVersion, fetchSplashData, fetchPrivacy, state} = context;
  const [modalVisible, setModalVisible] = useState(false);
  useEffect(() => {
    fetchPrivacy();
    checkVersion();
  }, []);

  useEffect(() => {
    if (state.appVersion) {
      checkAppVersion();
    }
  }, [state]);
  //====================== FORCE UPDATE  CHECK ========================
  const checkAppVersion = () => {
    const serverVersion = state.appVersion.version;
    console.log({serverVersion});

    if (version.vn < serverVersion && state.appVersion.isRequired) {
      setModalVisible(true);
    } else {
      checkUserAndFetchData();
    }
  };
  //====================== FORCE UPDATE  CHECK ========================

  const checkUserAndFetchData = async () => {
    const token = await checkTokenInDevice();
    if (token) {
      fetchSplashData(() => navigation.navigate('Tabs'));
    } else {
      navigation.navigate('AuthFlow');
    }
  };
  const onCancel = () => {
    // setModalVisible(false);
  };
  const cancelBtn = () => {
    BackHandler.exitApp();
  };
  const acceptBtn = () => {
    Linking.openURL(state.appVersion.link);
  };
  return (
    <View style={styles.container}>
      <Image style={styles.image} source={image} />
      <View style={styles.textHolder}>
        <Text style={[styles.text, h1]}>{SAFIR_CARD}</Text>
        <Text style={[styles.text, h4]}>{INSTALLMENT_NETWORK_TITILE}</Text>
      </View>
      <Text style={[styles.text, h4, {position: 'absolute', bottom: 70}]}>
        شرکت آتیه پردازان سفیر گیل
      </Text>
      <Image resizeMode={'stretch'} source={drLogo} style={styles.drLogo} />
      <CustomDialog
        status={3}
        state={modalVisible}
        title={FORSE_UPDATE_TITLE}
        exButton={EXIT_USER}
        oncancel={onCancel}
        accept={acceptBtn}
        ButtonAction={cancelBtn}
      />
    </View>
  );
};
export default SplashScreen;
