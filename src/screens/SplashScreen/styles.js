import {StyleSheet, Dimensions} from 'react-native';
import {MAIN_COLOR} from '../../values/Colors';
import {centerAll, h4, iranSans, h2, fWhite} from '../../values/Theme';
const {width, height} = Dimensions.get('screen');
export default styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: MAIN_COLOR,
    ...centerAll,
  },
  image: {
    width: 170,
    height: 170,
  },
  textHolder: {
    marginTop: 24,
    ...centerAll,
  },
  text: {
    ...iranSans,
    ...fWhite,
  },
  drLogo: {
    position: 'absolute',
    bottom: 30,
    width: width / 4,
    height: width / 16,
  },
});
