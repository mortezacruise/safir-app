import React, {useEffect, useState, useRef, createRef, useContext} from 'react';
import {View, StyleSheet, Image, Text, ImageBackground} from 'react-native';
import Geolocation from 'react-native-geolocation-service';
import CedarMaps from '@cedarstudios/react-native-cedarmaps';
import Permissions from 'react-native-permissions';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
const FINE_LOCATION = 'android.permission.ACCESS_FINE_LOCATION';
import {featureCollection, feature} from '@turf/helpers';
import {Context as DataContext} from '../../context/DataContext';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Spacer from '../../components/Spacer';
import {iranSans, h5, h6, fWhite} from '../../values/Theme';
import {navigate} from '../../../navigationRef';
import {MAIN_COLOR} from '../../values/Colors';
import FastImage from 'react-native-fast-image';
import {Image as LazyImage} from 'react-native-elements';
import {Indicator} from '../../components/Indicator';
import CustomDialog from '../../components/Modal';
import {LOCATION_REQ, EXIT_USER} from '../../values/Strings';
// import {Image as BImage} from 'native-base';
Icon.loadFont();

const LocationScreen = ({navigation}) => {
  const {state} = useContext(DataContext);
  // const mostView = state.homeScreenDataState.mostView.stores;
  // const newest = state.homeScreenDataState.newest.stores;
  // const todayOffer = state.homeScreenDataState.todayOffer.stores;

  // const newArray = [...mostView, ...newest, ...todayOffer];

  const [currLocation, setCurLocation] = useState([49.5891, 37.2682]);
  const [featureState, setFeature] = useState(featureCollection([]));
  const [selectedItem, setSelectedItem] = useState(null);
  const [newData, setNewData] = useState([]);
  const [modal, setModal] = useState(false);
  const [withState, setWithState] = useState({width: 70, borderRadius: 99});

  useEffect(() => {
    // setNewData(newArray);
  }, []);

  const parameter = {
    loc: navigation?.state?.params?.location || null,
    image: navigation?.state?.params?.image || null,
  };

  let phone = selectedItem?.owner?.phone && selectedItem?.owner?.phone[0];
  let mapRef = null;

  useEffect(() => {
    !parameter?.loc &&
      setTimeout(() => {
        requestPermissions();
      }, 2000);
    setTimeout(() => {
      setWithState({...withState, width: 71, borderRadius: 100});
    }, 10000);
  }, []);
  useEffect(() => {
    !parameter?.loc && onCULocPress(currLocation);
  }, [mapRef]);
  const onAcceptPermission = () => {
    Permissions.request(FINE_LOCATION);
  };
  const getCurrentLocation = async () => {
    Geolocation.getCurrentPosition(
      async (position) => {
        const newLocation = [
          position.coords.longitude,
          position.coords.latitude,
        ];
        setCurLocation(newLocation);
      },
      (error) => {},
      {enableHighAccuracy: true, timeout: 10000, maximumAge: 1000},
    );
  };
  //=========================== PERMISSION =====================
  const requestPermissions = (allow) => {
    Permissions.check(FINE_LOCATION).then((res) => {
      if (res === 'denied' || res === 'unavailable') {
        setModal(true);
      } else if (res === 'granted') getCurrentLocation();
    });
  };
  //=========================== PERMISSION =====================

  const onCULocPress = (loc, call) => {
    console.log({loc});

    mapRef?.flyTo(loc, 1000);
    setTimeout(() => {
      mapRef?.zoomTo(20, 2000);
      call && call();
    }, 1000);
  };
  const onMarkerSelect = (point, item) => {
    const loc = point.geometry.coordinates;
    onCULocPress(loc, () => setSelectedItem(item));
  };

  async function onPress(e) {
    const aFeature = feature(e.geometry);
    aFeature.id = `${Date.now()}`;
    // setFeature((prev) => featureCollection([...prev.features, aFeature]));
  }
  console.log({param: parameter.loc});

  const points = () => {
    if (parameter?.loc) {
      // onCULocPress(parameter.loc);
      return (
        <CedarMaps.PointAnnotation
          title={'salam'}
          selected={true}
          onSelected={onMarkerSelect}
          key="1"
          id="1"
          coordinate={parameter.loc}>
          <LazyImage
            borderRadius={100}
            source={{uri: parameter.image}}
            style={[
              styles.mapImage,
              {width: withState.width, borderRadius: withState.borderRadius},
            ]}
          />
        </CedarMaps.PointAnnotation>
      );
    } else {
      // setTimeout(() => {
      return state.allStores.map((item, index) => {
        const coords = [item.location.lng, item.location.lat];
        return (
          <CedarMaps.PointAnnotation
            title={'salam'}
            selected={true}
            onSelected={(i) => onMarkerSelect(i, item)}
            key={index}
            id={index.toString()}
            coordinate={coords}>
            <LazyImage
              borderRadius={100}
              source={{uri: item.slides[0]}}
              style={[
                styles.mapImage,
                {width: withState.width, borderRadius: withState.borderRadius},
              ]}
              key={index.toString()}
              PlaceholderContent={
                <Text style={[iranSans, {fontSize: 8, color: MAIN_COLOR}]}>
                  {item.title}
                </Text>
              }
            />
          </CedarMaps.PointAnnotation>
        );
      });
      // }, 3000);
    }
  };
  const onCardPress = () => {
    navigate('ClubsScreen', {clubParams: selectedItem});
  };
  return (
    <View style={{flex: 1}}>
      <CedarMaps.Map
        onPress={onPress}
        style={styles.map}
        contentInset={10}
        clientId={'baman-17709868195994349693'}
        clientSecret={
          'cEEg5GJhbWFu8PuRjo20szHVcox1PCTwlWeG01IkoasCzTDWP-Dlmj8='
        }
        mapStyle={'style://streets-light'}
        centerCoordinate={parameter.loc ? parameter.loc : currLocation}>
        <CedarMaps.Camera
          animationMode={'flyTo'}
          pointerEvents={'box-only'}
          zoomLevel={14}
          centerCoordinate={parameter.loc ? parameter.loc : currLocation}
          ref={(ref) => (mapRef = ref)}
        />
        {points()}
        <CedarMaps.PointAnnotation
          selected={true}
          onSelected={onMarkerSelect}
          key="2"
          id="2"
          coordinate={currLocation}>
          <Icon name="map-marker" size={30} color={MAIN_COLOR} />
        </CedarMaps.PointAnnotation>
      </CedarMaps.Map>
      {selectedItem && (
        <TouchableOpacity style={styles.selectedItem} onPress={onCardPress}>
          <View style={styles.textHolder}>
            <Text style={styles.text}>{selectedItem.title}</Text>
            <Spacer color="white" />
            <Text style={styles.text}>{selectedItem.owner.title}</Text>
            <Spacer color="white" />

            <Text style={styles.text}>{phone}</Text>
          </View>
          <FastImage
            source={{uri: selectedItem?.slides[0]}}
            style={styles.selectedItemImage}
          />
        </TouchableOpacity>
      )}
      <CustomDialog
        state={modal}
        status={1}
        title={LOCATION_REQ}
        exButton={EXIT_USER}
        ButtonAction={() => setModal(false)}
        oncancel={() => setModal(false)}
        accept={onAcceptPermission}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  map: {
    flex: 1,
    width: '100%',
    height: '100%',
  },
  icon: {},
  selectedItem: {
    width: '95%',
    backgroundColor: '#d7d7d7',
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    elevation: 1,
    borderRadius: 8,
    marginBottom: 8,
  },
  selectedItemImage: {
    width: 100,
    minHeight: 100,
    borderRadius: 8,
  },
  textHolder: {
    flexGrow: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    padding: 8,
  },
  text: {
    ...iranSans,
    marginVertical: 4,
    ...h6,
  },
  mapImage: {
    height: 70,
    // backgroundColor: MAIN_COLOR,
    alignItems: 'center',
    justifyContent: 'center', // backgroundColor: 'red',
    // zIndex: 2000,
  },
});
export default LocationScreen;
//BEZAN ROO NAGHSHE VA NOGHTE BENDAZ
{
  /* <CedarMaps.ShapeSource
onPress={onSourseLayerPress}
hitbox={{width: 30, height: 30}}
id="symbolLocationSource"
shape={featureState}>
<CedarMaps.SymbolLayer
  id="symbolLocationSymbols"
  minZoomLevel={1}
  style={{
    iconImage: exampleIcon,
    iconAllowOverlap: true,
    iconSize: 0.09,
  }}
/>
</CedarMaps.ShapeSource> */
}
// const onSourseLayerPress = ({features, coordinates, point}) => {
//     'You pressed a layer here are your features:',
//     features,
//     coordinates,
//     point,
//   );
// };
