import React, {useEffect, useState} from 'react';
import {View, Text, FlatList} from 'react-native';
import {getMoreSpecial} from '../../api/get/getMoreSpecial';
import {Indicator} from '../../components/Indicator';
import DiscountRow from '../../screens/CategoryScreen/SearchRow';
import {centerAll, Flex, h4, iranSans} from '../../values/Theme';
import {NO_DATA} from '../../values/Strings';
const MoreStoresInfinity = ({navigation}) => {
  const param = navigation.state.params;

  const [page, setPage] = useState(1);
  const [dataArr, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [pages, setPagses] = useState(null);
  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
    setLoading(true);
    getMore();
  }, [page]);

  const getMore = async () => {
    const res = await getMoreSpecial(param.data, page);

    if (res.data) {
      setData((prev) => [...prev, ...res.data.docs]);
      !pages && setPagses(res.data.pages);
      setLoading(false);
    } else {
      setLoading(false);
    }
  };
  const onRefresh = async () => {
    setRefreshing(true);
    const res = await getMoreSpecial(param.data, 1);
    if (res.data) {
      setData(res.data.docs);
      setPage(1);
      setRefreshing(false);
    } else {
      setRefreshing(false);
    }
  };
  if (dataArr.length > 0) {
    return (
      <FlatList
        contentContainerStyle={{
          flexDirection: 'column',
          margin: 2,
        }}
        onEndReachedThreshold={0.01}
        initialNumToRender={5}
        data={dataArr}
        renderItem={({item, index}) => {
          return <DiscountRow searchItem={item} index={index} />;
        }}
        keyExtractor={(item, index) => {
          return item._id;
        }}
        onEndReached={(reach) => {
          page <= pages && setPage((prev) => prev + 1);
        }}
        ListFooterComponent={loading && <Indicator />}
        onRefresh={onRefresh}
        refreshing={refreshing}
      />
    );
  } else {
    return (
      <View style={[centerAll, Flex]}>
        <Indicator />
      </View>
    );
  }
};
export default MoreStoresInfinity;
