import React, {useState, useEffect} from 'react';
import {View, FlatList, Text} from 'react-native';
import {Flex, centerAll, iranSans, h4} from '../../../../values/Theme';
import DiscountsRow from '../../SearchRow';
import {NO_DATA} from '../../../../values/Strings';
import {Indicator} from '../../../../components/Indicator';
import {getAllCategoryData} from '../../../../api/get/getAllCatrgoryData';

const TabContent = ({data}) => {
  const [page, setPage] = useState(1);
  const [dataArr, setData] = useState(data.stores.docs);
  const [loading, setLoading] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  useEffect(() => {
    if (page > 1) {
      setLoading(true);
      getMore();
    }
  }, [page]);
  const onRefresh = async () => {
    setRefreshing(true);
    const res = await getAllCategoryData();
    if (res.data) {
      setData(res.data.stores.docs);
      setRefreshing(false);
      setPage(1);
    }
    setRefreshing(false);
  };
  const getMore = async () => {
    const res = await getAllCategoryData(page);
    if (res.data) {
      setData((prev) => [...prev, ...res.data.stores.docs]);
      setLoading(false);
    } else {
      setLoading(false);
    }
  };

  if (dataArr.length > 0) {
    return (
      <FlatList
        contentContainerStyle={{
          flexDirection: 'column',
          margin: 2,
        }}
        onEndReachedThreshold={0.01}
        initialNumToRender={5}
        data={dataArr}
        renderItem={({item, index}) => {
          return <DiscountsRow searchItem={item} index={index} />;
        }}
        keyExtractor={(item, index) => {
          return item._id;
        }}
        onEndReached={(reach) => {
          page <= data.stores.pages && setPage((prev) => prev + 1);
        }}
        ListFooterComponent={loading && <Indicator />}
        refreshing={refreshing}
        onRefresh={onRefresh}
      />
    );
  } else {
    return (
      <View style={[centerAll, Flex]}>
        <Text style={[iranSans, h4]}>{NO_DATA}</Text>
      </View>
    );
  }
};

export default TabContent;
