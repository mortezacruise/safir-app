import React, {useContext, useEffect, useState} from 'react';
import {View, Text} from 'react-native';
import IntroSlider from '../../../components/IntroSlider';
import TabContent from './TabContent';
import {Indicator} from '../../../components/Indicator';
import {getAllCategoryData} from '../../../api/get/getAllCatrgoryData';
import {NO_DATA} from '../../../values/Strings';
import {Flex, centerAll, iranSans, h5, mV16} from '../../../values/Theme';
import Icon from 'react-native-vector-icons/AntDesign';
// import Slider from '../../../components/Slider';
const AllCategories = (props) => {
  const [data, setData] = useState(null);
  const [erro, setError] = useState(false);
  useEffect(() => {
    getData();
  }, []);
  const getData = async () => {
    erro && setError(false);
    const res = await getAllCategoryData();
    if (res.data) {
      setData(res.data);
    } else {
      setError(true);
    }
  };
  if (erro) {
    return (
      <View style={[Flex, centerAll]}>
        <Text style={[iranSans, h5, mV16]}>{NO_DATA}</Text>
        <Icon name="sync" size={30} onPress={getData} />
      </View>
    );
  }
  //===========================CONTEXT =========================
  if (!data) {
    return <Indicator />;
  }

  return (
    <View style={{flex: 1, backgroundColor: '#EEEEEE'}}>
      <IntroSlider slides={data?.slides || []} type={'cat'} />
      <TabContent data={data} type={'tabCategory'} />
    </View>
  );
};

export default AllCategories;
