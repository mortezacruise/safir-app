import React, {useEffect, useState, useContext} from 'react';
import {BackHandler, View, Text, FlatList} from 'react-native';
import {Context as DataContext} from '../../context/DataContext';
import {Indicator} from '../../components/Indicator';
import CatDataTab from './CatDataTab';
import IntroSlider from '../../components/IntroSlider';
import TabContent from './CatDataTab/TabContent';
import {NO_DATA} from '../../values/Strings';
import {Flex, centerAll, iranSans, h5, mV16} from '../../values/Theme';
import {getCategoriesData} from '../../api/get/getCategoriesData';
import Icon from 'react-native-vector-icons/AntDesign';

const CategoryScreen = ({navigation}) => {
  const param = navigation.state?.params?.cat;
  const [erro, setError] = useState(false);
  const [data, setData] = useState([]);
  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    erro && setError(false);
    const res = await getCategoriesData(param);
    if (res.data) {
      setData(res.data);
    } else {
      setError(true);
    }
  };
  if (erro) {
    return (
      <View style={[Flex, centerAll]}>
        <Text style={[iranSans, h5, mV16]}>{NO_DATA}</Text>
        <Icon name="sync" size={30} onPress={getData} />
      </View>
    );
  }
  if (data.length === 0) {
    return <Indicator />;
  }
  return (
    <View style={{flex: 1, backgroundColor: '#EEEEEE'}}>
      <IntroSlider slides={data.slides || []} type={'cat'} />
      <TabContent data={data} />
    </View>
  );
};
CategoryScreen.navigationOptions = () => {
  return {
    gesturesEnabled: false,
  };
};
export default CategoryScreen;
