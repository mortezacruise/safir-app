import {Dimensions, StyleSheet} from 'react-native';

export default styles = StyleSheet.create({
  searchRowContainer: {
    flex: 1,
    flexDirection: 'row-reverse',
    margin: 4,
    backgroundColor: 'white',
    elevation: 1,
    borderRadius: 5,
    overflow: 'hidden',
  },
  searchAvatar: {
    width: Dimensions.get('screen').width / 4,
    height: Dimensions.get('screen').width / 4,
  },
  titleContainer: {
    marginTop: 8,
    marginRight: 8,
  },
  percent: {
    position: 'absolute',
    left: 0,
    top: 0,
    backgroundColor: 'red',
    borderTopLeftRadius: 8,
    padding: 4,
    color: 'white',
  },
  priceContainer: {
    alignItems: 'center',
    margin: 4,
  },
  realPrice: {
    color: 'rgba(0,0,0,0.5)',
    textDecorationLine: 'line-through',
  },
  newPrice: {
    color: 'red',
  },
  disTitle: {
    width: Dimensions.get('screen').width / 2,
  },
  disDescription: {
    width: Dimensions.get('screen').width / 2,
  },
});
