import React, {useState, useContext, useMemo} from 'react';
import {View} from 'react-native';
import {Flex} from '../../../values/Theme';
import TabContent from './TabContent';
import {Context as DataContext} from '../../../context/DataContext';
const CatDataTab = () => {
  const context = useContext(DataContext);
  const {state} = context;
  const data = state.homeScreenDataState.mostView.stores;
  return useMemo(() => {
    return (
      <View style={[Flex, {marginTop: 8}]}>
        <TabContent data={data} />
      </View>
    );
  }, [data]);
};

export default CatDataTab;
