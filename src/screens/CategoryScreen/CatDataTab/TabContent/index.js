import React, {useState, useEffect} from 'react';
import {View, FlatList, Text} from 'react-native';
import {Flex, centerAll, iranSans, h4} from '../../../../values/Theme';
import DiscountsRow from '../../SearchRow';
import {NO_DATA} from '../../../../values/Strings';
import {getCategoriesData} from '../../../../api/get/getCategoriesData';
import {Indicator} from '../../../../components/Indicator';
import {screenWidth} from '../../../../values/Constants';

const TabContent = ({data}) => {
  const [page, setPage] = useState(1);
  const [dataArr, setData] = useState(data.stores.docs);
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    if (page > 1 && page <= data.stores.pages) {
      setLoading(true);
      getMore();
    }
  }, [page]);

  const getMore = async () => {
    const res = await getCategoriesData(data.categoryId, page);

    if (res.data) {
      setData((prev) => [...prev, ...res.data.stores.docs]);
      setLoading(false);
    } else {
      setLoading(false);
    }
  };

  if (dataArr.length > 0) {
    const getItemLayout = (data, index) => ({
      length: screenWidth / 4,
      offset: (screenWidth / 4) * index,
      index,
    });
    return (
      <FlatList
        getItemLayout={getItemLayout}
        maxToRenderPerBatch={10}
        windowSize={10}
        initialNumToRender={5}
        contentContainerStyle={{
          flexDirection: 'column',
          margin: 2,
        }}
        onEndReachedThreshold={0.01}
        initialNumToRender={5}
        data={dataArr}
        renderItem={({item, index}) => {
          return <DiscountsRow searchItem={item} index={index} />;
        }}
        keyExtractor={(item, index) => {
          return item._id;
        }}
        onEndReached={(reach) => {
          page <= data.stores.pages && setPage((prev) => prev + 1);
        }}
        ListFooterComponent={loading && <Indicator count={4} size={20} />}
      />
    );
  } else {
    return (
      <View style={[centerAll, Flex]}>
        <Text style={[iranSans, h4]}>{NO_DATA}</Text>
      </View>
    );
  }
};

export default TabContent;
