import axios from '../AxiosConfigue';
import {NO_INTERNET, SMT_WRONG} from '../../values/Strings';
export const toggleBookmark = async (id) => {
  return axios
    .post('/bookmark', {id})
    .then((res) => {
      return res.data.MSG;
    })
    .catch((e) => {
      if (e.message === 'Network Error') {
        return NO_INTERNET;
      } else {
        return SMT_WRONG;
      }
    });
};
