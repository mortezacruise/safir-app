import axios from '../AxiosConfigue';
export const postSearchValue = async (value) => {
  return axios
    .get(`/search/${value}`)
    .then((res) => {
      return {data: res.data};
    })
    .catch((e) => {
      return {e};
    });
};
