import axios from '../AxiosConfigue';
export const getAllClubsData = async () => {
  return axios
    .get('/club')
    .then((res) => res.data)
    .catch((e) => e);
};
