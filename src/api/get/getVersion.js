import axios from '../AxiosConfigue';
export const getVersion = () => {
  return axios
    .get('version')
    .then((res) => {
      return {data: res.data};
    })
    .catch((e) => {
      return {e};
    });
};
