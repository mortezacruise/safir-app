import axios from '../AxiosConfigue';
export const getCategoriesData = async (catId, page = 1) => {
  return axios
    .get(`/category/${catId}/${page}`)
    .then((res) => res)
    .catch((e) => e);
};
