import axios from '../AxiosConfigue';

export const getAllCategoryData = async (page = 1) => {
  return axios
    .get('store/' + page)
    .then((res) => res)
    .catch((e) => e);
};
