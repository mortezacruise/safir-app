import axios from '../AxiosConfigue';
export const getAllDiscounts = async () => {
  return axios
    .get('/discount')
    .then((res) => res.data)
    .catch((e) => e);
};
