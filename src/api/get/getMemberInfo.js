import axios from '../AxiosConfigue';
export const getMemberInfo = async () => {
  return axios
    .get('/profile')
    .then((res) => {
      return {data: res.data};
    })
    .catch((e) => {
      return {e};
    });
};
