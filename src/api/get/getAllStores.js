import axios from '../AxiosConfigue';

export const getAllStores = async () => {
  return axios
    .get('store')
    .then((res) => res)
    .catch((e) => e);
};
