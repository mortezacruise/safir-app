import axios from '../AxiosConfigue';
export const getMoreSpecial = async (special, page = 1) => {
  return axios
    .get(`/specialClubs/${special}/${page}`)
    .then((res) => res)
    .catch((e) => e);
};
