import axios from '../AxiosConfigue';
export const getSplashData = async () => {
  return axios
    .get('/splashscreen')
    .then((res) => {
      return {data: res.data};
    })
    .catch((e) => {
      return {error: e};
    });
};
