import axios from '../AxiosConfigue';

export const getUserBalance = (id) => {
  return axios
    .get(`/usertransactions/${id}`)
    .then((res) => {
      return {data: res.data};
    })
    .catch((e) => {
      return {e};
    });
};
