import axios from '../AxiosConfigue';

export const getPravicy = async () => {
  return axios
    .get('/privacy')
    .then((res) => {
      return {data: res.data};
    })
    .catch((e) => {
      return {e};
    });
};
