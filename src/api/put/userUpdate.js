import axios from '../AxiosConfigue';
export const userUpdate = async (data) => {
  return axios
    .put('/profile ', data)
    .then((res) => {
      return {data: res.data};
    })
    .catch((e) => {
      return {e};
    });
};
