import createDataContext from './createDataContext';
import {LoginApi} from '../api/post/LoginApi';
import AxiosConf from '../api/AxiosConfigue';
import AsyncStorage from '@react-native-community/async-storage';
import {navigate} from '../../navigationRef';
import {
  ENTER_11_NUmber,
  TRY_AGAIN,
  ENTER_6_NUMBER,
  ENTERED_CODE_IS_WRONG,
} from '../values/Strings';
const ADD_ERROR = 'add_error';
const SIGN_IN = 'signin';
const CLEAR_ERROR_MESSAGE = 'clear_error_message';
const SIGN_OUT = 'signout';
const MSG_SEND = 'msg_send';
const LOADING = 'loading';
const EDITE_NUMBER = 'edit_number';
const TOKEN = 'token';
const authReducer = (state, action) => {
  switch (action.type) {
    case 'add_error':
      return {...state, errorMessage: action.payload};
    case 'signin':
      return {errorMessage: '', token: action.payload};
    case 'clear_error_message':
      return {...state, errorMessage: ''};
    case 'signout':
      return {token: null, errorMessage: ''};
    case 'msg_send':
      return {...state, showCode: true};
    case 'loading':
      return {...state, loading: action.payload};
    case 'edit_number':
      return {...state, showCode: action.payload};
    case 'token':
      return {...state, token: action.payload};
    default:
      return state;
  }
};
// const add = (a, b) => a + b;
const clearErrorMessage = (dispatch) => () =>
  dispatch({type: 'clear_error_message'});

const tryLoacalSignIn = (dispatch) => async () => {
  const token = await AsyncStorage.getItem('token');
  if (token) {
    dispatch({type: 'signin', payload: token});
    navigate('TrackList');
  } else {
    navigate('Singup');
  }
};
const signup = (dispatch) => async (phoneNumber) => {
  dispatch({
    type: 'add_error',
    payload: '',
  });
  dispatch({type: 'loading', payload: true});
  if (phoneNumber.length > 10) {
    const response = await LoginApi({phoneNumber});
    console.log({response});
    if (response === 2016) {
      dispatch({type: 'msg_send'}), dispatch({type: 'loading', payload: false});
    } else {
      dispatch({
        type: 'add_error',
        payload: TRY_AGAIN,
      });
      dispatch({type: 'loading', payload: false});
    }
  } else {
    dispatch({
      type: 'add_error',
      payload: ENTER_11_NUmber,
    });
    dispatch({type: 'loading', payload: false});
  }
};

const signin = (dispatch) => async ({email, password}) => {
  try {
    navigate('TrackList');
  } catch (error) {
    console.log({error});
    dispatch({
      type: 'add_error',
      payload: 'somthing went wrong with signin ',
    });
  }
};

const signout = (dispatch) => async () => {
  await AsyncStorage.removeItem('token');
  dispatch({type: 'signout'});
  navigate('loginFlow');
};
const enterToken = (dispatch) => async (token, phoneNumber, callBack) => {
  dispatch({type: 'add_error', payload: ''});
  console.log('eter');

  dispatch({type: 'loading', payload: true});
  if (token.length >= 6) {
    const response = await AxiosConf.put('/signup', {
      phoneNumber: phoneNumber,
      verificationCode: token,
    })
      .then(async (res) => {
        console.log({res});

        await AsyncStorage.setItem('token', res.data.token),
          dispatch({type: 'token', payload: res.data.token});
        dispatch({type: 'loading', payload: false});
        if (callBack) callBack();
      })
      .catch((e) => {
        if (e.response.data.Errors === 1014) {
          dispatch({type: 'add_error', payload: ENTERED_CODE_IS_WRONG});
        }
        console.log({e}), dispatch({type: 'loading', payload: false});
      });
    console.log(response);
  } else {
    dispatch({type: 'add_error', payload: ENTER_6_NUMBER});
    dispatch({type: 'loading', payload: false});
  }
};

const editNumber = (dispatch) => () => {
  dispatch({type: 'edit_number', payload: false});
};

export const {Provider, Context} = createDataContext(
  authReducer,
  {
    signin,
    signout,
    signup,
    clearErrorMessage,
    tryLoacalSignIn,
    enterToken,
    editNumber,
  },
  {token: null, errorMessage: '', showCode: false, loading: false},
);
