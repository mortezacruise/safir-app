import createDtateContext from './createDataContext';
import {getAllDiscounts} from '../api/get/getAllDiscounts';

import {getAllClubsData} from '../api/get/getAllClubsData';
import {getCategories} from '../api/get/getCategories';
import {getCategoriesData} from '../api/get/getCategoriesData';
import {getSplashData} from '../api/get/getSplashData';
import {getVersion} from '../api/get/getVersion';
import {getPravicy} from '../api/get/getPrivacy';
import {getAllStores} from '../api/get/getAllStores';
const FETCH_ALL_DISCOUNTS = 'fetch_all_discounts';
const GET_CLUBS_DATA = 'get_all_clubs_data';
const GET_CATEGORIES = 'get_categories';
const GET_CAT_DATA = 'get-cat-data';
const SET_CATEGORIES = 'set_catrgoties';
const SET_USER = 'set_user';
const SET_HOMESCREEN = 'set_home_screen';
const CITY_FILTER = 'city_filter';
const TYPE_FILTER = 'type_filter';
const SET_GIFT_ARRAY = 'set_gift_array';
const SET_SPIN_TIME = 'spin';
const SET_APP_VERSION = 'SET_APP_VERSION';
const DELETE_CAT_DATA = 'DELETE_CAT_DATA';
const PRIVACY = 'PRIVACY';
const SET_ALL_STORE = 'SET_ALL_STORE';
const DataContext = (state, action) => {
  switch (action.type) {
    case 'home_data':
      return {...state, homeData: action.payload};
    case FETCH_ALL_DISCOUNTS:
      return {...state, allDiscounts: action.payload};
    case GET_CLUBS_DATA:
      return {...state, allClubsData: action.payload};
    case GET_CATEGORIES:
      return {...state, categories: action.payload};
    case GET_CAT_DATA:
      return {...state, catData: action.payload};
    case SET_CATEGORIES:
      return {...state, categoiresState: action.payload};
    case SET_USER:
      return {...state, userState: action.payload};
    case SET_HOMESCREEN:
      return {...state, homeScreenDataState: action.payload};
    case CITY_FILTER:
      return {...state, cityFilter: action.payload};
    case TYPE_FILTER:
      return {...state, typeFilter: action.payload};
    case SET_GIFT_ARRAY:
      return {...state, giftArray: action.payload};
    case SET_SPIN_TIME:
      return {...state, timeSpin: action.payload};
    case SET_APP_VERSION:
      return {...state, appVersion: action.payload};
    case DELETE_CAT_DATA:
      return {...state, catData: null};
    case PRIVACY: {
      return {...state, privacy: action.payload};
    }
    case SET_ALL_STORE:
      return {...state, allStores: action.payload};
    default:
      return state;
  }
};

const getData = (dispatch) => (data) => {
  dispatch({type: 'home_data', payload: data});
};
const fetchAlldiscounts = (dispatch) => async (page) => {
  const res = await getAllDiscounts();

  dispatch({type: FETCH_ALL_DISCOUNTS, payload: res});
};
const fetchClubsData = (dispatch) => async (page) => {
  const res = await getAllClubsData();
  console.log({res});

  dispatch({type: GET_CLUBS_DATA, payload: res});
};
const fetchCaregories = (dispatch) => async (data) => {
  const res = await getCategories();
  dispatch({type: GET_CATEGORIES, payload: res});
};
const fetchCategoriesData = (dispatch) => async (disId) => {
  dispatch({type: DELETE_CAT_DATA});
  const res = await getCategoriesData(disId);
  console.log({res});
  if (res.data) {
    console.log('dashtim');
    dispatch({type: GET_CAT_DATA, payload: res.data});
  }
};
const fetchSplashData = (dispatch) => async (callBack) => {
  const resSplashData = await getSplashData();
  console.log({resSplashData});

  if (resSplashData.data) {
    dispatch({type: SET_CATEGORIES, payload: resSplashData.data.categories});
    dispatch({type: SET_USER, payload: resSplashData.data.user});
    dispatch({type: SET_HOMESCREEN, payload: resSplashData.data.homeScreen});
    callBack && callBack();
  } else {
    console.log({resSplashData});
  }
};
const setCityFilter = (dispatch) => (item) => {
  dispatch({type: CITY_FILTER, payload: item});
};
const setTypeFilter = (dispatch) => (item) => {
  dispatch({type: TYPE_FILTER, payload: item});
};
const checkVersion = (dispatch) => async () => {
  const res = await getVersion();
  console.log({res});

  dispatch({type: SET_APP_VERSION, payload: res.data});
};
const fetchPrivacy = (dispatch) => async () => {
  const res = await getPravicy();
  res.data && dispatch({type: PRIVACY, payload: res.data});
};
const getAllStore = (dispatch) => async () => {
  const res = await getAllStores();
  console.log({res});
  if (res.data) {
    dispatch({type: SET_ALL_STORE, payload: res.data});
  }
};
export const {Provider, Context} = createDtateContext(
  DataContext,
  {
    getData,
    fetchCategoriesData,
    fetchCaregories,
    fetchSplashData,
    setTypeFilter,
    setCityFilter,
    checkVersion,
    fetchPrivacy,
    getAllStore,
  },
  {
    homeData: null,
    categories: [],
    allDiscounts: null,
    allClubsData: null,
    categories: null,
    catData: null,
    categoiresState: [],
    userState: null,
    homeScreenDataState: null,
    cityFilter: '',
    typeFilter: '',
    giftArray: [],
    timeSpin: null,
    maxSpin: null,
    gift: null,
    appVersion: null,
    privacy: '',
    allStores: [],
  },
);
