import createDtateContext from './createDataContext';
import {getMemberInfo} from '../api/get/getMemberInfo';
import {userUpdate} from '../api/put/userUpdate';

const USER_INFO = 'user_info';
const UserDataContext = (state = inisialSatate, action) => {
  switch (action.type) {
    case USER_INFO:
      return {...state, userInfo: action.payload};
    default:
      return state;
  }
};
const inisialSatate = {
  userInfo: null,
};
const getMemberInformation = (dispatch) => async () => {
  const resUser = await getMemberInfo();
  console.log({resUser});
  if (resUser.data) {
    dispatch({type: USER_INFO, payload: resUser.data});
  }
};
const putUserInfo = (dispatch) => async (data, callBack) => {
  const res = await userUpdate(data);
  res.data && callBack();
};
export const {Provider, Context} = createDtateContext(
  UserDataContext,
  {getMemberInformation, putUserInfo},
  inisialSatate,
);
