import React from 'react';
import {StyleSheet, View} from 'react-native';
import AppIntroSlider from './AppIntroSlider';
import Ionicons from 'react-native-vector-icons/FontAwesome5';
import AsyncStorage from '@react-native-community/async-storage';

const styles = StyleSheet.create({
  buttonCircle: {
    width: 40,
    height: 40,
    backgroundColor: 'rgba(0, 0, 0, .2)',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const IntroSlider = ({slides, type}) => {
  const renderNextButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Ionicons
          name="arrow-left"
          color="rgba(255, 255, 255, .9)"
          size={24}
          style={{backgroundColor: 'transparent'}}
        />
      </View>
    );
  };
  const renderDoneButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Ionicons
          name="home"
          color="rgba(255, 255, 255, .9)"
          size={24}
          style={{backgroundColor: 'transparent'}}
        />
      </View>
    );
  };
  const onDone = async (e) => {};
  return (
    <AppIntroSlider
      type={type}
      slides={slides}
      renderDoneButton={renderDoneButton}
      renderNextButton={renderNextButton}
      onDone={onDone}
    />
  );
};
export default IntroSlider;
