import React from 'react';
import {View, Image, Dimensions} from 'react-native';
const {height, width} = Dimensions.get('screen');
const DefaultSlide = (props) => {
  return (
    <View>
      <Image
        resizeMode="stretch"
        source={{uri: props.type === 'cat' ? props.item.image : props.item}}
        style={{width: width, height: height / 3}}
      />
    </View>
  );
};
export default DefaultSlide;
