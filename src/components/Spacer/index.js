import React, {useEffect} from 'react';
import {View, Text, StyleSheet} from 'react-native';
const Spacer = ({color = '#d7d7d7'}) => {
  return <View style={[styles.container, {backgroundColor: color}]} />;
};
export default Spacer;
const styles = StyleSheet.create({
  container: {
    width: '90%',
    height: 1,
    alignSelf: 'center',
  },
});
