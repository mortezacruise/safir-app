import React, {useEffect, useState} from 'react';
import {View, Text} from 'react-native';
import {SliderBox} from 'react-native-image-slider-box';
const Slider = ({data}) => {
  const images = [
    'https://source.unsplash.com/1024x768/?nature',
    'https://source.unsplash.com/1024x768/?water',
    'https://source.unsplash.com/1024x768/?girl',
    'https://source.unsplash.com/1024x768/?tree', // Network image
  ];
  const [withState, setWith] = useState(null);
  const onLayout = (e) => {
    setWith(e.nativeEvent.layout.width);
  };
  return (
    <View style={{width: '100%', height: 200}} onLayout={onLayout}>
      <SliderBox
        images={images}
        sliderBoxHeight={200}
        onCurrentImagePressed={(index) =>
          console.warn(`image ${index} pressed`)
        }
        parentWidth={withState}
      />
    </View>
  );
};
export default Slider;
