import React, {useState, useEffect} from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import {Dialog} from 'react-native-simple-dialogs';
import {
  Flex,
  rMain,
  centerAll,
  pad8,
  m8,
  bgMainColor,
  h5,
  iranSans,
  fWhite,
  fRow,
  Tac,
  h4,
  s100,
  bgRed300,
  r8,
} from '../../values/Theme';
import Icon from 'react-native-vector-icons/AntDesign';
const CustomDialog = ({
  state = false,
  title = '',
  accept,
  status = 1,
  oncancel,
  ButtonAction,
  exButton,
  acceptButton = 'تایید',
}) => {
  useEffect(() => {
    setShowAlert(state);
  }, [state, status]);
  useEffect(() => {
    switch (status) {
      case 1:
        return setIconNam('check'), setColor('green');
      case 2:
        return setIconNam('check'), setColor('red');
      case 3:
        return setIconNam('warning'), setColor('orange');
    }
  });
  const _onAccept = () => {
    setShowAlert(false);
    accept();
  };
  const _onCancel = () => {
    oncancel();
  };
  const cancelBtn = () => {
    setShowAlert(false);
    ButtonAction();
  };

  const [showAlert, setShowAlert] = useState(false);
  const [iconName, setIconNam] = useState('check');
  const [color, setColor] = useState('green');

  return (
    <Dialog
      dialogStyle={{borderRadius: 25, borderColor: color, borderWidth: 2}}
      animationType={'fade'}
      titleStyle={[iranSans, h4, Tac]}
      visible={state && showAlert}
      title={title}
      onTouchOutside={() => {
        _onCancel();
      }}>
      <View style={[centerAll]}>
        <Icon
          style={[{marginBottom: 16}]}
          name={iconName}
          size={50}
          color={color}
        />
        <View style={[centerAll]}>
          <TouchableOpacity
            onPress={_onAccept}
            style={[{width: 100}, centerAll, pad8, m8, bgMainColor, r8]}>
            <Text style={[fWhite, iranSans, rMain, h5]}>{acceptButton}</Text>
          </TouchableOpacity>
          {exButton ? (
            <TouchableOpacity
              onPress={cancelBtn}
              style={[{width: 100, height: 32}, centerAll, r8]}>
              <Text style={[iranSans, rMain, h5]}>{exButton}</Text>
            </TouchableOpacity>
          ) : null}
        </View>
      </View>
    </Dialog>
  );
};
export default CustomDialog;
