import React, {useEffect, useRef, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  KeyboardAvoidingView,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {Input} from 'native-base';
import {LIGHT_GRAY, MAIN_COLOR} from '../../values/Colors';
import {h5, iranSans, centerAll} from '../../values/Theme';
import SearchList from './SearchList';
// import { postSearchValue } from "../../API/postSearchValue";
const Data = [
  {title: 1, slides: ['https://picsum.photos/id/237/536/354']},
  {title: 1, slides: ['https://picsum.photos/id/237/536/354']},
  {title: 1, slides: ['https://picsum.photos/id/237/536/354']},
  {title: 1, slides: ['https://picsum.photos/id/237/536/354']},
  {title: 1, slides: ['https://picsum.photos/id/237/536/354']},
  {title: 1, slides: ['https://picsum.photos/id/237/536/354']},
];
const SearchContainer = ({visible, navigation}) => {
  const [data, setData] = useState({club: [], discount: []});
  let inputref = useRef();
  useEffect(() => {
    if (navigation) {
      navigation.addListener('didFocus', (payload) => {
        if (inputref) {
          inputref.focus();
        }
      });
    }
  }, [navigation]);
  const onChangeInput = async (value) => {
    // if (value.length >= 3) {
    //   const respost = await postSearchValue(value);
    //   const club = respost.club;
    //   const discount = respost.discount;
    //   setData({ club, discount });
    // }
  };

  return (
    <View style={[styles.container]}>
      <Icon
        name="arrow-right"
        color={MAIN_COLOR}
        style={styles.Icon}
        size={20}
        onPress={() => navigation.goBack()}
      />
      <TextInput
        ref={(inp) => (inputref = inp)}
        style={styles.Input}
        placeholder="دنبال چی میگردی"
        placeholderTextColor={LIGHT_GRAY}
        onChangeText={onChangeInput}
      />
      <KeyboardAvoidingView behavior="height">
        <SearchList
          navigation={navigation}
          data={data.discount}
          title={'تخفیف ها'}
        />
        <SearchList
          navigation={navigation}
          data={data.club}
          title={'باشگاه ها'}
        />
      </KeyboardAvoidingView>
    </View>
  );
};
export default SearchContainer;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
  },
  Input: {
    width: '90%',
    height: 70,
    ...h5,
    ...iranSans,
    borderRadius: 8,
    textAlign: 'right',
    borderWidth: 2,
    marginTop: 12,
    padding: 12,
    paddingRight: 50,
    borderColor: MAIN_COLOR,
  },
  Icon: {
    position: 'absolute',
    top: 35,
    right: 35,
    zIndex: 2000,
  },
});
{
}
